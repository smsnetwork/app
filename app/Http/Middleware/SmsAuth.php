<?php

namespace App\Http\Middleware;

use Closure;

class SmsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('uuid')) {
            return redirect(route('home'));
        }

        return $next($request);
    }
}
