<?php

namespace App\Http\Middleware;

use App\Services\Clients\SmsApiClient;
use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $smsApiClient = new SmsApiClient();
        $uuid = $request->session()->get('uuid');
        $studentProfile = $smsApiClient->call('GET', 'students/' . $uuid);
        if(!$studentProfile) {
            throw new NotFoundHttpException;
        }
        $request->user = $studentProfile;
        return $next($request);
    }
}
