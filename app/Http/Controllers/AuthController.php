<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

use App\Services\Clients\SmsApiClient;

class AuthController extends Controller
{
    protected $smsApiClient;

    public function __construct(SmsApiClient $smsApiClient)
    {
        $this->smsApiClient = $smsApiClient;
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'student_id' => 'required',
            'password' => 'required',
        ]);

        try {

            $student = $this->smsApiClient->authenticate([
                'student_id' => $request->get('student_id'),
                'password' => $request->get('password'),
            ]);

            $request->session()->put('uid', $student['id']);
            $request->session()->put('uuid', $student['uuid']);
            $request->session()->put('token', $student['token']);
            $request->session()->flash('success', 'You have logged in successfully');

            return redirect(route('dashboard'));

        } catch (\Exception $e) {
            return redirect(route('home'))
                ->with('error', $e->getMessage())
                ->withInput($request->all());
        }
    }

    public function logout(Request $request)
    {
        if($request->session()->exists('uid')) {
            $request->session()->flush();
            $request->session()->flash('success', 'You have logged out successfully');
            return redirect(route('home'));
        }
        return redirect(route('home'));
    }
}
