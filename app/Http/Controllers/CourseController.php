<?php

namespace App\Http\Controllers;

use App\Services\Clients\SmsApiClient;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CourseController extends Controller
{
    protected $smsApiClient;

    public function __construct(SmsApiClient $smsApiClient)
    {
        $this->smsApiClient = $smsApiClient;
    }

    public function register(Request $request)
    {
        $user = $request->user;
        if (!$user) {
            throw new NotFoundHttpException;
        }
        if($user->current_department == 'null') {
            $departmentCourses = [];
        } else {
            $departmentCourses = $this->smsApiClient->call('GET', 'departments/' . $user->current_department->uuid . '?include=courses');
        }
        return view('course.register', [
            'user' => $user,
            'departmentCourses' => $departmentCourses,
            'level' => ($user->current_level != 'null') ? $user->current_level : 'null',
            'department' => ($user->current_department != 'null') ? $user->current_department : 'null',
        ]);
    }
}
