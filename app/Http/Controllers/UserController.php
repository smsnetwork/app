<?php

namespace App\Http\Controllers;

use App\Exceptions\SmsApiException;
use App\Http\Requests\Apply\PostBiodataRequest;
use App\Services\Clients\SmsApiClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $smsApiClient;

    public function __construct(SmsApiClient $smsApiClient)
    {
        $this->smsApiClient = $smsApiClient;
    }

    public function dashboard(Request $request)
    {
        $user = $request->user;
        return view('student.dashboard', [
            'user' => $user,
        ]);
    }

    public function profile(Request $request)
    {
        $user = $request->user;
        return view('student.profile', [
            'user' => $user,
        ]);
    }

    public function apply()
    {
        return view('apply.home', [

        ]);
    }

    public function biodata(Request $request)
    {
        $departments = $this->smsApiClient->call('GET', 'departments', []);
        return view('apply.biodata', [
            'departments' => $departments,
        ]);
    }

    public function postBioData(PostBiodataRequest $request)
    {
        try {
            $user = $this->smsApiClient->call('POST', '/users', [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => [
                    'last_name' => $request->get('surname'),
                    'first_name' => $request->get('firstname'),
                    'middle_name' => $request->get('middlename'),
                    'mobile' => $request->get('mobile'),
                    'email' => $request->get('email'),
                    'gender' => $request->get('gender'),
                    'birthday' => $request->get('date_of_birth'),
                    'password' => $request->get('surname'),
                    'student_id' => strtoupper($request->get('surname')[0]) . rand(100000, 999999),
                    'type' => 'student',
                ],
            ]);
            $request->session()->set('uid', $user->id);
            $request->session()->set('uuid', $user->uuid);

            return redirect(route('createEducation'));

        } catch (SmsApiException $e) {
            if ($e->hasValidationErrors()) {
                return redirect(route('createBiodata'))
                    ->withErrors($e->getValidationErrors())
                    ->withInput();
            }

            return redirect(route('createBiodata'))
                ->with('error', $e->getErrorMessage());
        }
    }

    public function education(Request $request)
    {
        try {
            $subjects = $this->smsApiClient->call('GET', '/subjects');

            return view('apply.education', [
                'subjects' => $subjects,
            ]);

        } catch (SmsApiException $e) {
            return redirect(route('createEducation'))
                ->with('error', $e->getErrorMessage());
        }
    }

    public function postEducation(Request $request)
    {
        $sittings = $request->get('sitting');
        $response = [];
        $i = 1;
        if ($request->session()->has('uuid')) {
            try {
                foreach ($sittings as $sitting) {
                    $this->smsApiClient->call('POST',
                        '/students/' . $request->session()->get('uuid') . '/ssce', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'json' => [
                                'school_name' => $sitting['school_name'],
                                "number" => $sitting['exam_number'],
                                "year" => $sitting['exam_year'],
                                "sitting" => $i++,
                                "pin" => $sitting['result_pin'],
                                "serial" => $sitting['result_serial'],
                                "type" => $sitting['exam_type'],
                                "results" => $sitting['results'] ?? []
                            ],
                        ]);
                }

                return redirect(route('createRelationships'));

            } catch (SmsApiException $e) {
                return redirect(route('createEducation'))
                    ->with('error', $e->getErrorMessage());
            }

        }

        return redirect(route('createEducation'));

    }

    public function relationship(Request $request)
    {
        return view('apply.relationship', [

        ]);
    }

    public function postRelationship(Request $request)
    {
        $nexkOfKins = $request->get('nk');
        if ($request->session()->has('uuid')) {
            try {
                foreach ($nexkOfKins as $nexkOfKin) {
                    $this->smsApiClient->call('POST',
                        '/students/' . $request->session()->get('uuid') . '/associates', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'json' => [
                                'name' => $nexkOfKin['name'],
                                "mobile" => $nexkOfKin['mobile'],
                                "email" => $nexkOfKin['email'],
                                "address" => $nexkOfKin['address'],
                                "type" => $nexkOfKin['type'],
                                "relationship" => $nexkOfKin['relationship'],
                            ],
                        ]
                    );
                }

                return redirect(route('createRelationships'));
            } catch (SmsApiException $e) {
                return redirect(route('createRelationships'))
                    ->with('error', $e->getErrorMessage());
            }
        }
        return redirect(route('home'));
    }

    public function finalSteps(Request $request)
    {
        if(!$request->session()->has('uuid')) {
            return redirect(route('home'))->with('error', 'Please login');
        }
        try {
            $user = $this->smsApiClient->call('GET', 'students/' . $request->session()->get('uuid'), [
                'query' => [
                    'include' => ['associates', 'ssce']
                ]
            ]);

        } catch(SmsApiException $e) {
            return redirect(route('finalSteps'))
                ->with('error', $e->getErrorMessage());
        }

        return view('apply.final', [
            'user' => $user
        ]);
    }

    public function postFinalSteps(Request $request)
    {
        dd($request->all());
    }
}
