<?php

namespace App\Http\Requests\Apply;

use Illuminate\Foundation\Http\FormRequest;

class PostBiodataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => ['required', 'string'],
            'firstname' => ['required', 'string'],
            'middlename' => ['string'],
            'email' => ['required', 'email', 'string'],
            'mobile' => ['required', 'string'],
            'gender' => ['required', 'in:male,female'],
            'date_of_birth' => ['required'],
            'permanent_address' => ['required'],
            'address' => ['required'],
        ];
    }
}
