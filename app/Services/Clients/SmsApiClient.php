<?php

namespace App\Services\Clients;

use App\Exceptions\SmsApiException;
use App\Services\Repository\UserRepository;
use App\Services\Repository\UserService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 10/3/16
 * Time: 2:47 PM
 */
class SmsApiClient
{
    public function authenticate($credentials)
    {
        $student = $this->createAuthenticationClient($credentials);
        if($student) {
            $options = [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $student->id,
                    'client_secret' => env('API_CLIENT_SECRET'),
                    'username' => $student->email,
                    'password' => $credentials['password']
                ]
            ];
            $accessToken = $this->call('POST', 'oauth/token', $options);
            $response = [
                'id' => $student->id,
                'uuid' => $student->uuid,
                'token' => $accessToken->access_token,
            ];
        }
        return $response;
    }

    public function call($method, $url, $options = [], $client = null)
    {
        if ($client === null) {
            $client = $this->createResourceClient();
        }
        try {
            $response = $client->request($method, $url, $options);
            $data = \GuzzleHttp\json_decode($response->getBody()->getContents());
            if (isset($data->data) && (is_array($data->data) || is_object($data->data))) {
                return $data->data;
            }
        } catch (ClientException $e) {
            throw SmsApiException::clientException($e);
        } catch (ServerException $e) {
            throw SmsApiException::serverException($e);
        }
        return $data;
    }

    protected function client()
    {
        return $this->createResourceClient();
    }
    protected function createResourceClient()
    {
        $client = new Client([
            'base_uri' => env('API_BASE_URL'),
            'headers' => [
                'Authorization' => 'Bearer ' . session('token')
            ]
        ]);
        return $client;
    }

    protected function createAuthenticationClient($credentials)
    {
        try {
            $client = new Client([
                'base_uri' => env('API_BASE_URL'),
                'json' => $credentials
            ]);
            $response = $client->request('POST', 'students/login');
            $student = \GuzzleHttp\json_decode($response->getBody()->getContents());
            return $student->data;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}