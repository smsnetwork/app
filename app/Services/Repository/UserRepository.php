<?php
namespace App\Services\Repository;

use App\Services\Clients\SmsApiClient;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 10/3/16
 * Time: 3:01 PM
 */
class UserRepository
{
    protected $smsApiClient;

    public function __construct(SmsApiClient $smsApiClient)
    {
        $this->smsApiClient = $smsApiClient;
    }

    public function getCurrentUser()
    {
        try {
            $studentUuid = session()->get('uuid');
            $user = $this->smsApiClient->call('GET', 'students/' . $studentUuid);
            if($user) {
                return $user->data;
            }
            throw NotFoundHttpException::class;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}