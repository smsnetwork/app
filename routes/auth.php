<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('students/login', [
    'as' => 'login',
    'uses' => 'AuthController@getLogin'
]);

Route::post('students/login', [
    'as' => 'studentPostLogin',
    'uses' => 'AuthController@postLogin'
]);

Route::get('logout', [
    'as' => 'logout',
    'uses' => 'AuthController@logout'
]);