<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 10/3/16
 * Time: 5:51 PM
 */

Route::get('/', [
    'as' => 'home',
    'uses' => 'AuthController@getLogin'
]);

Route::get('/sessions', function() {
    dd(session()->all());
});