<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 10/3/16
 * Time: 5:52 PM
 */


Route::group(['middleware' => ['user.session','sms.auth']], function (){
    Route::get('register/course', [
        'as' => 'register.course',
        'uses' => 'CourseController@register'
    ]);
});