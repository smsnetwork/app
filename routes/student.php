<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 10/3/16
 * Time: 5:52 PM
 */


Route::group(['middleware' => ['user.session','sms.auth']], function (){
    Route::get('dashboard', [
        'as' => 'dashboard',
        'uses' => 'UserController@dashboard'
    ]);
    Route::get('profile', [
        'as' => 'student.profile',
        'uses' => 'UserController@profile'
    ]);
});

//Unsecure endpoint for student registration
Route::get('apply', [
    'as' => 'studentApplication',
    'uses' => 'UserController@apply'
]);

Route::get('create/biodata', [
    'as' => 'createBiodata',
    'uses' => 'UserController@biodata'
]);

Route::post('post/biodata', [
    'as' => 'postBiodata',
    'uses' => 'UserController@postBioData'
]);

Route::get('create/education', [
    'as' => 'createEducation',
    'uses' => 'UserController@education'
]);


//endpoint to post educational record
Route::post('post/education', [
    'as' => 'postEducation',
    'uses' => 'UserController@postEducation'
]);

Route::get('create/relationships', [
    'as' => 'createRelationships',
    'uses' => 'UserController@relationship'
]);

Route::post('post/relationships', [
    'as' => 'postRelationship',
    'uses' => 'UserController@postRelationship'
]);

Route::get('final/steps', [
    'as' => 'finalSteps',
    'uses' => 'UserController@finalSteps'
]);

Route::get('post/final/steps', [
    'as' => 'postFinalSteps',
    'uses' => 'UserController@postFinalSteps'
]);