'use strict'

import noty from 'noty'
import $ from 'jquery'

let Notify = function () {
}

Notify.prototype.show = function (type, message, options = {}) {
  let payload = {
    type,
    text: message,
    timeout: 2000,
    layout: 'bottomRight'
  }

  return noty($.extend(payload, options))
}

let helpers = ['alert', 'error', 'success', 'info', 'warning']
helpers.forEach(helper => {
  Notify.prototype[helper] = function (message, options = {}) {
    return this.show(helper, message, options)
  }
})

Notify.install = function (Vue, options) {
  Vue.notify = new Notify()
  Object.defineProperties(Vue.prototype, {
    $notify: {
      get () {
        return Vue.notify
      }
    }
  })
}

export default Notify