'use strict'

import Notify from './plugins/noty'

Vue.use(Notify)

import course from './course'

import education from './apply/education'