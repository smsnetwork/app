'use strict'

import Vue from 'vue'
import log from './filters/log'

export default new Vue({
  el: '#course',
  data: {
    department: Sms.department
  },
  computed: {
    courses () {
      return this.department.courses.data.map(course => {
        course.selected = course.pivot.optional === '0'
        return course
      })
    },
    selectedCourses () {
      let courses = this.courses
      courses = courses.filter(course => {
        return course.selected === true
      })

      return courses
    }
  },
  created() {

  },
  methods: {
    clickSubmitCourses(ev) {
      ev.preventDefault()
      log(this.courses)
    }
  },
  filters: {
    log
  }
})
