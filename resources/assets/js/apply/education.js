'use strict'

import Vue from 'vue'
import _ from 'lodash'

const courseList = Sms.subjects;

export default new Vue({
  el: '#education',
  data: {
    fieldsFirst:{
      course: '',
      grade: null
    },
    fieldsSecond:{
      course: '',
      grade: null
    },
    grades: [
      'A1', 'B2', 'B3', 'C4', 'C5', 'C6', 'D7', 'E8', 'F9'
    ],
    selectedCoursesFirst: [],
    selectedCoursesSecond: [],
    show: false
  },

  computed: {
    availableCoursesFirst () {
      let selectedIds = _.map(this.selectedCoursesFirst, 'id')
      return _.clone(courseList).filter(course => {
        return selectedIds.indexOf(course.id) === -1
      })
    },
    availableCoursesSecond () {
      let selectedIds = _.map(this.selectedCoursesSecond, 'id')
      return _.clone(courseList).filter(course => {
        return selectedIds.indexOf(course.id) === -1
      })
    }
  },

  methods: {
    clickRemoveCourseFirst (id) {
      let idx = _.findIndex(this.selectedCoursesFirst, {id: Number(id)})

      this.selectedCoursesFirst.splice(idx, 1)
    },

    clickRemoveCourseSecond (id) {
      let idx = _.findIndex(this.selectedCoursesSecond, {id: Number(id)})

      this.selectedCoursesSecond.splice(idx, 1)
    },

    clickAddCourseFirst () {
      if (this.fieldsFirst.course === '') {
        return void 0
      }

      let course = _.find(courseList, {id: Number(this.fieldsFirst.course)})

      this.selectedCoursesFirst.push({
        id: course.id,
        name: course.name,
        grade: this.fieldsFirst.grade
      })

      this.fieldsFirst.course = ''
      this.fieldsFirst.grade = null
    },

    clickAddCourseSecond () {
      if (this.fieldsSecond.course === '') {
        return void 0
      }

      let course = _.find(courseList, {id: Number(this.fieldsSecond.course)})

      this.selectedCoursesSecond.push({
        id: course.id,
        name: course.name,
        grade: this.fieldsSecond.grade
      })

      this.fieldsSecond.course = ''
      this.fieldsSecond.grade = null
    },

    addSitting () {
      return this.show = true
    },

    removeSitting () {
      return this.show = false
    }
  }
})
