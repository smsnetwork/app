<div class="form-group">
  <div class="col-md-12">
    <h3 class="text-muted"> Second Sitting </h3>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label"> School Name : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[2][school_name]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Exam Number : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[2][exam_number]" type="text" placeholder="Type your password"
           data-parsley-equalto="#passwordinput" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Year : </label>
  <div class="col-sm-2">
    <input class="form-control" name="sitting[2][exam_year]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
  <label class="col-sm-1 control-label"> Exam : </label>
  <div class="col-sm-3">
    <select name="sitting[2][exam_type]" class="form-control">
      <option value="waec">WAEC</option>
      <option value="neco">NECO</option>
      <option value="nabteb">NABTEB</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Result Pin : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[2][result_pin]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Result Serial : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[2][result_serial]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-6" data-index="">
    <div v-if="selectedCoursesSecond.length === 0">
      <div class="text-warning">No Subject has been selected</div>
    </div>
    <div v-else>
      <div v-for="(course, idx) in selectedCoursesSecond">
        <div class="col-sm-8">
          <input type="hidden" class="form-control" :name="`sitting[2][results][${idx}][subject_id]`"
                 :value="course.id">
          <span class="form-control">@{{ course.name }}</span>
        </div>
        <div class="col-sm-2">
          <input type="hidden" class="form-control" :name="`sitting[2][results][${idx}][grade]`"
                 v-bind:value="course.grade">
          <span class="form-control">@{{ course.grade }}</span>
        </div>
        <div class="col-sm-2">
          <a href="javascript:void(0)" @click="clickRemoveCourse(course.id)">
          <i class="fa fa-times text-danger"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label"> Subject : </label>
  <div class="col-sm-3">
    <select class="form-control" v-model="fieldsSecond.course">
      <option :value="''">-- Select Subject</option>
      <option v-for="course in availableCoursesSecond"
              :value="course.id">
        @{{ course.name }}
      </option>
    </select>
  </div>
  <label class="col-sm-1 control-label"> Grade : </label>
  <div class="col-sm-2">
    <select class="form-control" v-model="fieldsSecond.grade">
      <option :value="''">Grade</option>
      <option v-for="grade in grades"
              :value="grade">
        @{{ grade }}
      </option>
    </select>
  </div>
  <div class="col-sm-2">
    <a href="javascript:void(0)" @click="clickAddCourseSecond()">
    <i class="fa fa-plus text-primary"></i> Add Course
    </a>
  </div>
</div>