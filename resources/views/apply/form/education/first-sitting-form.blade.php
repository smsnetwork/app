<div class="form-group">
  <div class="col-md-12">
    <h3 class="text-muted"> First Sitting </h3>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label"> School Name : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[1][school_name]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Exam Number : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[1][exam_number]" type="text" placeholder="Type your password"
           data-parsley-equalto="#passwordinput" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Year : </label>
  <div class="col-sm-2">
    <input class="form-control" name="sitting[1][exam_year]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
  <label class="col-sm-1 control-label"> Exam : </label>
  <div class="col-sm-3">
    <select name="sitting[1][exam_type]" class="form-control">
      <option value="waec">WAEC</option>
      <option value="neco">NECO</option>
      <option value="nabteb">NABTEB</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Result Pin : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[1][result_pin]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"> Result Serial : </label>
  <div class="col-sm-6">
    <input class="form-control" name="sitting[1][result_serial]" type="text" placeholder="Type your Name"
           data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-6" data-index="">
    <div v-if="selectedCoursesFirst.length === 0">
      <div class="text-warning">No Subject has been selected</div>
    </div>
    <div v-else>
      <div v-for="(course, idx) in selectedCoursesFirst">
        <div class="col-sm-8">
          <input type="hidden" class="form-control" :name="`sitting[1][results][${idx}][subject_id]`"
                 :value="course.id">
          <span class="form-control">@{{ course.name }}</span>
        </div>
        <div class="col-sm-2">
          <input type="hidden" class="form-control" :name="`sitting[1][results][${idx}][grade]`"
                 v-bind:value="course.grade">
          <span class="form-control">@{{ course.grade }}</span>
        </div>
        <div class="col-sm-2">
          <a href="javascript:void(0)" @click="clickRemoveCourseFirst(course.id)">
          <i class="fa fa-times text-danger"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label"> Subject : </label>
  <div class="col-sm-3">
    <select class="form-control" v-model="fieldsFirst.course">
      <option :value="''">-- Select Subject</option>
      <option v-for="course in availableCoursesFirst"
              :value="course.id">
        @{{ course.name }}
      </option>
    </select>
  </div>
  <label class="col-sm-1 control-label"> Grade : </label>
  <div class="col-sm-2">
    <select class="form-control" v-model="fieldsFirst.grade">
      <option :value="''">Grade</option>
      <option v-for="grade in grades"
              :value="grade">
        @{{ grade }}
      </option>
    </select>
  </div>
  <div class="col-sm-2">
    <a href="javascript:void(0)" @click="clickAddCourseFirst()">
    <i class="fa fa-plus text-primary"></i> Add Course
    </a>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-6">
    <div v-if="!show">
      <a href="javascript:void(0)" class="pull-right btn btn-primary" @click="addSitting()">
      <i class="fa fa-plus"></i>
      Add Second Sitting
      </a>
    </div>
    <div v-else>
      <a href="javascript:void(0)" class="pull-right btn btn-danger" @click="removeSitting()">
      <i class="fa fa-plus"></i>
      Remove Second Sitting
      </a>
    </div>
  </div>
</div>