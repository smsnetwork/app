<form class="form-horizontal form-bordered form-wizard clearfix" action="{{ route('postBiodata') }}"
      id="wizard-validate" role="application" method="post">
  <div class="steps clearfix">
    <ul role="tablist">
      <li role="tab" class="first current" aria-disabled="false" aria-selected="true">
        <a id="wizard-validate-t-0" href="{{ route('createBiodata') }}">
          <span class="current-info audible">current step: </span><span class="number">1.</span>
          <span class="title"> Biodata </span>
        </a>
      </li>
      <li role="tab" class="disabled" aria-disabled="true">
        <a id="wizard-validate-t-1" href="{{ route('createEducation') }}">
          <span class="number">2.</span>
          <span class="title"> Educational Record </span>
        </a>
      </li>
      <li role="tab" class="disabled" aria-disabled="true">
        <a id="wizard-validate-t-2" href="{{ route('createRelationships') }}">
          <span class="number">3.</span>
          <span class="title"> Guardian Record </span>
        </a>
      </li>
      <li role="tab" class="disabled last" aria-disabled="true">
        <a id="wizard-validate-t-3" href="{{ route('finalSteps') }}" aria-controls="wizard-validate-p-3">
          <span class="number">4.</span>
          <span class="title"> Final </span>
        </a>
      </li>
    </ul>
  </div>
  <div class="content clearfix">
    <!-- Wizard Container 1 -->
    <div class="wizard-title title current" id="wizard-validate-h-0" tabindex="-1"> Registration</div>
    <div class="wizard-container body current" id="wizard-validate-p-0" role="tabpanel"
         aria-labelledby="wizard-validate-h-0" aria-hidden="false" style="display: block;">
      <div class="form-group">
        <div class="col-md-12">
          <p class="text-muted"> Fill your biodata form </p>
        </div>
      </div>
      <div class="form-group {{ $errors->has('surname') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Surname : </label>
        <div class="col-sm-6">
          <input class="form-control" name="surname" type="text" placeholder="Type your Name"
                 value="{{ old('surname') }}">
          @if($errors->has('surname'))
            <small class="help-block">{{ $errors->first('surname') }}
            </small>
          @endif
        </div>
      </div>
      <div class="form-group {{ $errors->has('middlename') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Middle Name : </label>
        <div class="col-sm-6">
          <input class="form-control" name="middlename" type="text" placeholder="Type your Name" value="{{ old('middlename') }}">
          @if($errors->has('middlename'))
            <small class="help-block"
                   style="">{{ $errors->first('middlename') }}
            </small>
          @endif
        </div>
      </div>

      <div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> First Name : </label>
        <div class="col-sm-6">
          <input class="form-control" name="firstname" type="text" placeholder="Type your Name"
                 data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
          @if($errors->has('firstname'))
            <small class="help-block" value="{{ old('firstname') }}"
                   style="">{{ $errors->first('firstname') }}
            </small>
          @endif
        </div>
      </div>

      <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Email Address : </label>
        <div class="col-sm-6">
          <input class="form-control" name="email" type="email" placeholder="Type your Name" value="{{ old('email') }}">
          @if($errors->has('email'))
            <small class="help-block"
                   style="">{{ $errors->first('email') }}
            </small>
          @endif
        </div>
      </div>

      <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Mobile : </label>
        <div class="col-sm-6">
          <input class="form-control" name="mobile" type="text" value="{{ old('mobile') }}">
          @if($errors->has('mobile'))
            <small class="help-block"
                   style="">{{ $errors->first('mobile') }}
            </small>
          @endif
        </div>
      </div>

      <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Gender : </label>
        <div class="col-sm-6">
          <select class="form-control" name="gender">
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
          @if($errors->has('gender'))
            <small class="help-block"
                   style="">{{ $errors->first('gender') }}
            </small>
          @endif
        </div>
      </div>


      <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : '' }} date">
        <label class="col-sm-2 control-label"> Date of Birth : </label>
        <div class="col-sm-6">
          <input type="text" name="date_of_birth" value="{{ old('date_of_birth') }}" class="form-control">
          @if($errors->has('date_of_birth'))
            <small class="help-block"
                   style="">{{ $errors->first('date_of_birth') }}
            </small>
          @endif
        </div>
      </div>
      <div class="form-group {{ $errors->has('permanent_address') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Permanent Address : </label>
        <div class="col-sm-6">
          <textarea class="form-control" name="permanent_address" value="{{ old('permanent_address') }}"></textarea>
          @if($errors->has('permanent_address'))
            <small class="help-block"
                   style="">{{ $errors->first('permanent_address') }}
            </small>
          @endif
        </div>
      </div>
      <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
        <label class="col-sm-2 control-label"> Address 2 : </label>
        <div class="col-sm-6">
          <textarea class="form-control" name="address" value="{{ old('address') }}"></textarea>
          @if($errors->has('address'))
            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="address" data-bv-result="INVALID"
                   style="">{{ $errors->first('address') }}
            </small>
          @endif
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="actions clearfix">
    <ul role="menu" aria-label="Pagination">
      <li class="disabled" aria-disabled="true">
        <a href="#previous" role="menuitem" class="btn btn-default" disabled>Previous</a>
      </li>
      <li aria-hidden="false" aria-disabled="false">
        <button class="btn btn-default">Next</button>
    </ul>
  </div>
</form>