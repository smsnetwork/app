@for($i = 1; $i < 3; $i++)
  <div class="form-group">
    <div class="col-md-12">
      <p class="text-muted"> Next of Kin Details {{ $i }} </p>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"> Name : </label>
    <div class="col-sm-6">
      <input class="form-control" name="nk[{{ $i }}][name]" type="text" placeholder="Type your Name"
             data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"> Mobile : </label>
    <div class="col-sm-6">
      <input class="form-control" name="nk[{{ $i }}][mobile]" type="text" placeholder="Type your Mobile"
             data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label"> Email : </label>
    <div class="col-sm-6">
      <input class="form-control" name="nk[{{ $i }}][email]" type="email" placeholder="Type your Email"
             data-parsley-range="[4, 10]" data-parsley-group="order" data-parsley-required="">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label"> Address : </label>
    <div class="col-sm-6">
      <textarea name="nk[{{ $i }}][address]" class="form-control"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"> Relationship : </label>
    <div class="col-sm-3">
      <input type="text" name="nk[{{ $i }}][relationship]" class="form-control" placeholder="Relationship">
    </div>
    <label class="col-sm-1 control-label"> Type : </label>
    <div class="col-sm-2">
      <select class="form-control" name="nk[{{ $i }}][type]">
        <option value="next_of_kin">Next of Kin</option>
        <option value="guardian">Guardian</option>
      </select>
    </div>
  </div>
@endfor