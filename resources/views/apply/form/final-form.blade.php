<form class="form-horizontal form-bordered form-wizard clearfix" action="{{ route('postFinalSteps') }}"
      id="wizard-validate"
      role="application">
  <div class="steps clearfix">
    <ul role="tablist">
      <li role="tab" class="disabled" aria-disabled="true">
        <a id="wizard-validate-t-0" href="{{ route('createBiodata') }}">
          <span class="current-info audible">current step: </span><span class="number">1.</span>
          <span class="title"> Biodata </span>
        </a>
      </li>
      <li role="tab" class="disabled last" aria-disabled="true">
        <a id="wizard-validate-t-1" href="{{ route('createEducation') }}">
          <span class="number">2.</span>
          <span class="title"> Educational Record </span>
        </a>
      </li>
      <li role="tab" class="disabled last" aria-disabled="true">
        <a id="wizard-validate-t-2" href="{{ route('createRelationships') }}">
          <span class="number">3.</span>
          <span class="title"> Guardian Record </span>
        </a>
      </li>
      <li role="tab" class="first current" aria-disabled="false" aria-selected="true">
        <a id="wizard-validate-t-3" href="{{ route('finalSteps') }}">
          <span class="current-info audible">current step: </span><span class="number">4.</span>
          <span class="title"> Final </span>
        </a>
      </li>
    </ul>
  </div>
  <div class="content clearfix">
    <!-- Wizard Container 1 -->
    <div class="wizard-title title current" id="wizard-validate-h-0" tabindex="-1"> Registration</div>
    <div class="wizard-container body current" id="wizard-validate-p-0" role="tabpanel"
         aria-labelledby="wizard-validate-h-0" aria-hidden="false" style="display: block;">
      <div class="form-group">
        <div class="col-md-12">
          <p class="text-muted"> Fill next of kin details </p>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label"> Name : </label>
        <div class="col-sm-6">
          <span class="form-control">{{ $user->name }}</span>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label"> Mobile : </label>
        <div class="col-sm-2">
          <span class="form-control">{{ $user->mobile }}</span>
        </div>
        <label class="col-sm-1 control-label"> Email : </label>
        <div class="col-sm-3">
          <span class="form-control">{{ $user->email }}</span>
        </div>
      </div>

      <h3>Next of Kin Details</h3>
      @if(count($user->associates))
        <table class="table table-hover table-vcenter">
          <thead>
          <tr>
            <th>S/No</th>
            <th>Name</th>
            <th class="text-center">Email</th>
            <th class="hidden-xs">Mobile</th>
            <th>Address</th>
            <th>Relationship</th>
          </tr>
          </thead>
          <tbody>
          @foreach($user->associates->data as $key => $associate)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>{{ $associate->name }}</td>
              <td>{{ $associate->email }}</td>
              <td>{{ $associate->mobile }}</td>
              <td>{{ $associate->address }}</td>
              <td>{{ $associate->relationship }}</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      @endif
      <h3>SSCE Result</h3>
      @if(count($user->ssce))
        <div class="">
          @foreach($user->ssce->data as $key => $ssce)
            <h4>Result {{ $ssce->sitting }}</h4>
            <div class="form-group">
              <label class="col-sm-2 control-label"> School Name : </label>
              <div class="col-sm-6">
                <span class="form-control">{{ $ssce->school }}</span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"> Sitting : </label>
              <div class="col-sm-6">
                <span class="form-control">{{ $ssce->sitting }}</span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"> School Name : </label>
              <div class="col-sm-6">
                <span class="form-control">{{ $ssce->school }}</span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"> Year : </label>
              <div class="col-sm-6">
                <span class="form-control">{{ $ssce->year }}</span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"> Results : </label>
              @if(count($ssce->ssceResult))
                <ul class="nav navbar-default">
                @foreach($ssce->ssceResult as $result)
                      <li>{{ $result->name }} - {{ $result->pivot->grade }} </li>
                @endforeach
                </ul>
              @endif
            </div>
          @endforeach
        </div>
      @endif
      <div class="form-group">
        <label class="col-sm-2 control-label"> Address : </label>
        <div class="col-sm-6">
          <span class="form-control">Address</span>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label"> Type : </label>
        <div class="col-sm-6">
          <span class="form-control">Type</span>
        </div>
      </div>
    </div>
  </div>
  <div class="actions clearfix">
    <ul role="menu" aria-label="Pagination">
      <li class="disabled" aria-disabled="true">
        <a href="#previous" role="menuitem" class="btn btn-default">Previous</a>
      </li>
      <li aria-hidden="false" aria-disabled="false">
        <a href="#next" role="menuitem" class="btn btn-default">Next</a></li>
      <li aria-hidden="true" style="display: none;">
        <a href="#finish" role="menuitem" class="btn btn-primary">Finish</a>
      </li>
    </ul>
  </div>
</form>