<form class="form-horizontal form-bordered form-wizard clearfix" action="{{ route('postRelationship') }}"
      id="wizard-validate" role="application" method="post">
  <div class="steps clearfix">
    <ul role="tablist">
      <li role="tab" class="disabled" aria-disabled="true">
        <a id="wizard-validate-t-0" href="{{ route('createBiodata') }}">
          <span class="current-info audible">current step: </span><span class="number">1.</span>
          <span class="title"> Biodata </span>
        </a>
      </li>
      <li role="tab" class="disabled last" aria-disabled="true">
        <a id="wizard-validate-t-1" href="{{ route('createEducation') }}">
          <span class="number">2.</span>
          <span class="title"> Educational Record </span>
        </a>
      </li>
      <li role="tab" class="first current" aria-disabled="false" aria-selected="true">
        <a id="wizard-validate-t-2" href="{{ route('createRelationships') }}">
          <span class="current-info audible">current step: </span><span class="number">3.</span>
          <span class="title"> Guardian Record </span>
        </a>
      </li>
      <li role="tab" class="disabled last" aria-disabled="true">
        <a id="wizard-validate-t-3" href="{{ route('finalSteps') }}">
          <span class="number">4.</span>
          <span class="title"> Final </span>
        </a>
      </li>
    </ul>
  </div>
  <div class="content clearfix">
    <!-- Wizard Container 1 -->
    <div class="wizard-title title current" id="wizard-validate-h-0" tabindex="-1"> Registration</div>
    <div class="wizard-container body current" id="wizard-validate-p-0" role="tabpanel"
         aria-labelledby="wizard-validate-h-0" aria-hidden="false" style="display: block;">
      @include('apply.form.relationship.nk-form')
    </div>
  </div>

  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="actions clearfix">
    <ul role="menu" aria-label="Pagination">
      <li class="disabled" aria-disabled="true">
        <a href="#previous" role="menuitem" class="btn btn-default">Previous</a>
      </li>
      <li aria-hidden="false" aria-disabled="false">
        <button class="btn btn-default">Next</button>
      <li aria-hidden="true" style="display: none;">
        <a href="#finish" role="menuitem" class="btn btn-primary">Finish</a>
      </li>
    </ul>
  </div>
</form>