@extends('layouts.default')

@section('title', 'Student Biodata')

@section('content')
  <div class="page-content">
    <section class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Final Form</h3>
      </div>
      <div class="panel-body">
        @include('apply.form.final-form', ['user' => $user])
      </div>
    </section>
  </div>
@endsection