@extends('layouts.default')

@section('title', 'New Application')

@section('content')
  <div class="profile-body">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title"> Instruction </h3>
      </div>
      <div class="panel-body" style="min-height: 500px;">
        <div class="list-group">
          <a href="#" class="list-group-item active">
            <h4 class="list-group-item-heading">List group item heading</h4>
            <p class="list-group-item-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie
              venenatis convallis.</p>
          </a>
          <a href="#" class="list-group-item">
            <h4 class="list-group-item-heading">List group item heading</h4>
            <p class="list-group-item-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie
              venenatis convallis.</p>
          </a>
          <a href="#" class="list-group-item">
            <h4 class="list-group-item-heading">List group item heading</h4>
            <p class="list-group-item-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie
              venenatis convallis.</p>
          </a>
        </div>
        <a href="{{ route('createBiodata') }}" class="btn btn-success pull-right">Proceed</a>
      </div>
    </div>
  </div>
@endsection