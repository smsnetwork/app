@extends('layouts.default')

@section('title', 'Student Biodata')

@section('content')
  <div id="education">
    <section class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Education Record</h3>
      </div>
      <div class="panel-body">
        @include('apply.form.education.education-form')
      </div>
    </section>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    window.Sms.subjects = {!! json_encode($subjects) !!}
  </script>
@overwrite