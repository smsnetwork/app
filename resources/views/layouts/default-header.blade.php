<header id="navbar" class="animated slideInDown">
  <div id="navbar-container" class="boxed">
    <!--Brand logo & name-->
    <!--================================-->
    <div class="navbar-header">
      <a href="#" class="navbar-brand">
        <i class="fa fa-cube brand-icon"></i>
        <div class="brand-title">
          <span class="brand-text">Rayan</span>
        </div>
      </a>
    </div>
    <!--================================-->
    <!--End brand logo & name-->
    <!--Navbar Dropdown-->
    <!--================================-->
    <div class="navbar-content clearfix">
      <ul class="nav navbar-top-links pull-right">
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End Fullscreen toogle button-->
        <!--User dropdown-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <li id="dropdown-user" class="dropdown">
          <a href="{{ route('studentApplication') }}" class="text-right">
            <span class="pull-right"></span>
            <div class="username hidden-xs">Apply</div>
          </a>
        </li>
        <li id="dropdown-user" class="dropdown">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
            <span class="pull-right"></span>
            <div class="username hidden-xs">Instruction</div>
          </a>
          <div class="dropdown-menu dropdown-menu-right with-arrow">
            <!-- User dropdown menu -->
            <ul class="head-list">
              <li>
                <a href="#"> <i class="fa fa-user fa-fw"></i> Fresher </a>
              </li>
              <li>
                <a href="#">  <i class="fa fa-user fa-fw"></i> Resturning </a>
              </li>
              <li>
                <a href="#">  <i class="fa fa-credit-card fa-fw"></i> Payment </a>
              </li>
              <li>
                <a href="#"> <i class="fa fa-book fa-fw"></i> Course </a>
              </li>
            </ul>
          </div>
        </li>
        <li id="dropdown-user" class="dropdown">
          <a href="{{ route('login') }}" class="text-right">
            <span class="pull-right"></span>
            <div class="username hidden-xs">Login</div>
          </a>
        </li>
      </ul>
    </div>
    <!--================================-->
    <!--End Navbar Dropdown-->
  </div>
</header>