<!Doctype html>
<html>
<head>
  <title>Portal | @yield('title')</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}">
</head>
<body>

<div id="container" class="effect mainnav-lg navbar-fixed mainnav-fixed">
  <div id="app">
    @include('layouts.app-header')
    <div class="boxed">
      <div id="content-container">
        @include('layouts.app-profile')
        @include('snippets.message')
        @include('snippets.alerts')
        @yield('content')
      </div>
      @include('layouts.app-sidebar')
      @include('layouts.app-aside')
    </div>
    <footer id="footer">
      @include('layouts.app-footer')
      <span class="glyphicon glyphicon-chevron-up"></span>
    </footer>
  </div>
</div>
<script type="text/javascript">
  window.Sms = {}
</script>
@section("scripts")@show
<script type="application/javascript" src="{{ asset('assets/js/vendor.js') }}"></script>
<script type="application/javascript" src="{{ asset('assets/js/app.js') }}"></script>
</body>
</html>