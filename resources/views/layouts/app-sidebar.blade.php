<nav id="mainnav-container">
  <div id="mainnav">
    <!--Menu-->
    <!--================================-->
    <div id="mainnav-menu-wrap">
      <div class="nano">
        <div class="nano-content" tabindex="0">
          <ul id="mainnav-menu" class="list-group">
            <!--Category name-->
            <li class="list-header">Student</li>
            <!--Menu list item-->
            <li class="active">
              <a href="javascript:void(0)" data-original-title="" title="" class="">
                <i class="fa fa-home"></i>
                <span class="menu-title">Dashboard</span>
                <i class="arrow"></i>
              </a>
              <!--Submenu-->

              <ul class="collapse pop-in" aria-expanded="false">
                <li><a href="{{ route('dashboard') }}"><i class="fa fa-caret-right"></i> Dashboard</a></li>
                <li><a href="{{ route('student.profile') }}"><i class="fa fa-caret-right"></i> Profile</a></li>
              </ul>
            </li>
            <!--Category name-->
            <li class="list-header">School</li>
            <!--Menu list item-->
            <li>
              <a href="#" data-original-title="" title="" class="">
                <i class="fa fa-th"></i>
                <span class="menu-title">Courses</span>
                <i class="arrow"></i>
              </a>
              <!--Submenu-->

              <ul class="collapse pop-in" aria-expanded="false">
                <li><a href="{{ route('register.course') }}"><i class="fa fa-caret-right"></i> Register </a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i> Manage </a></li>
              </ul>
            </li>
            <!--Menu list item-->
            <li>
              <a href="#" data-original-title="" title="" class="">
                <i class="fa fa-briefcase"></i>
                <span class="menu-title">Results</span>
                <i class="arrow"></i>
              </a>
              <!--Submenu-->

              <ul class="collapse pop-in" aria-expanded="false">
                <li><a href="#"><i class="fa fa-caret-right"></i> Check </a></li>
              </ul>
            </li>
            <!--Menu list item-->
            <li>
              <a href="#" data-original-title="" title="">
                <i class="fa fa-file"></i>
                <span class="menu-title">Payment</span>
                <i class="arrow"></i>
              </a>
              <!--Submenu-->

              <ul aria-expanded="true">
                <li><a href="#"><i class="fa fa-caret-right"></i> Make Payment </a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i> Transactions </a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="nano-pane" style="display: none;">
          <div class="nano-slider" style="height: 20px; transform: translate(0px, 0px);"></div>
        </div>
      </div>
    </div>
    <!--End menu-->
  </div>
</nav>