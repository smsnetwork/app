<!Doctype html>
<html>
<head>
  <title>Portal | @yield('title')</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}">
</head>
<body>
<div id="container" class="effect mainnav-lg navbar-fixed mainnav-fixed">
  <div id="app">
    @include('layouts.default-header')
    <div class="boxed">
      <div class="row">
        <div class="box-content">
          <div class="sms-content">
            @include('snippets.message')
            @include('snippets.alerts')
            @yield('content')
          </div>
        </div>
      </div>
    </div>
    <footer id="footer">
      @include('layouts.default-footer')
      <span class="glyphicon glyphicon-chevron-up"></span>
    </footer>
  </div>
</div>
<script type="text/javascript">
  window.Sms = {}
</script>
@section("scripts")@show
<script type="application/javascript" src="{{ asset('assets/js/vendor.js') }}"></script>
<script type="application/javascript" src="{{ asset('assets/js/app.js') }}"></script>
</body>
</html>