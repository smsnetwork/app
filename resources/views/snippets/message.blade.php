@if (Session::has('error'))
  <div class="alert alert-danger alert-dismissable no-radius">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ implode('<br />', (array) Session::get('error')) }}
  </div>
@endif
@if (Session::has('success'))
  <div class="alert alert-success alert-dismissable no-radius">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ implode('<br />', (array) Session::get('success')) }}
  </div>
@endif
@if (Session::has('message'))
  <div class="alert alert-warning alert-dismissable no-radius">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ implode('<br />', (array) Session::get('message')) }}
  </div>
@endif
@if (Session::has('warning'))
  <div class="alert alert-warning alert-dismissable no-radius">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ implode('<br />', (array) Session::get('warning')) }}
  </div>
@endif
@if(Session::has('info'))
  <div class="alert alert-info alert-dismissable no-radius">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    @foreach(Session::get('info') as $info)
      <li>{{$info}}</li>
    @endforeach
  </div>
@endif