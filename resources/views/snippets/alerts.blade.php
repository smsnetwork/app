@if($errors->count() > 0)
  <div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    @foreach($errors->all() as $error)
      <i class="fa fa-remove pr10"></i> {{$error}} <br>
    @endforeach
  </div>
@endif