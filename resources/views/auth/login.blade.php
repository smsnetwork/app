@extends('layouts.default')

@section('title', 'Welcome')

@section('content')
  <div class="col-md-7">
    <h4>Instruction</h4>
    <ul class="nav">
      <li>You are adviced to provide your</li>
      <li>You are adviced to provide your</li>
      <li>You are adviced to provide your</li>
      <li>You are adviced to provide your</li>
    </ul>
  </div>
  <div class="col-md-5">
    <div class="panel lock-box text-center">
      <div class="center"><img alt="" src="{{ asset('assets/img/user.png') }}" class="img-circle"></div>
      <h4> Hello User !</h4>
      <p class="text-center">Please login to Access your Account</p>
      <form action="{{ route('studentPostLogin') }}" method="post" class="form-horizontal">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
          <div class="text-left" {{ $errors->has('student_id') ? "has-error" : "" }}>
            <label class="text-muted">Student ID</label>
            <input id="signupInputEmail1" type="text" name="student_id" placeholder="Enter Student ID" class="form-control" required="">
          </div>
          <div class="text-left" {{ $errors->has('student_id') ? "has-error" : "" }}>
            <label for="signupInputPassword" class="text-muted">Password</label>
            <input id="signupInputPassword" type="password" name="password" placeholder="Password" class="form-control lock-input"
                   required="">
          </div>
          <br />
          <div class="pull-left pad-btm">
            <label class="form-checkbox form-icon form-text">
              <input type="checkbox"> Remember Me
            </label>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-block btn-primary">
            Sign In
          </button>
        </div>
      </form>
    </div>
  </div>
@endsection