@extends('layouts.app')

@section('title', $user->name)

@section('content')
  <div id="course">
    <div id="course-registration">
      <div class="pageheader">
        <h3><i class="fa fa-home"></i> Course Registration </h3>
        <div class="breadcrumb-wrapper">
          <span class="label">You are here:</span>
          <ol class="breadcrumb">
            <li><a href="#"> Home </a></li>
            <li class="active"> course table</li>
          </ol>
        </div>
      </div>
      <!--Page content-->
      <!--===================================================-->
      <div id="page-content">
        <div class="row">
          <div class="panel">
            <div class="panel-heading">
              <div class="panel-title pull-right">
                Department: {{ ($department != 'null') ? $department->name : 'No Department Yet' }},
                Year: {{ ($level != 'null') ? $level->name : 'No Level Yet' }}
              </div>
              <h3 class="panel-title">Courses</h3>
            </div>
            <!-- Striped Table -->
            <!--===================================================-->

            <div class="panel-body">
              <div class="table-responsive">
                <form action="" method="post">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Code</th>
                      <th>Title</th>
                      <th>Unit</th>
                      <th>Status</th>
                      <th>Semester</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @{{ selectedCourses | log }}
                    @if($level != 'null')
                      @if(count($departmentCourses))
                        <tr v-for="(course, idx) in courses">
                          {{--@{{ course | log }}--}}
                          <td>@{{ idx + 1 }}</td>
                          <td>@{{ course.code }}</td>
                          <td>@{{ course.name }}</td>
                          <td>@{{ course.pivot.unit }}</td>
                          <td>@{{ course.pivot.optional == '1' ? 'Core' : 'Elective' }}</td>
                          <td>@{{ course.pivot.semester }}</td>
                          <td>
                            <input type="checkbox" v-model="course.selected">
                          </td>
                        </tr>
                      @else
                        <tr>
                          <td>
                            <div class="alert alert-danger">No Courses For this department yet</div>
                          </td>
                        </tr>
                      @endif
                    @else
                      <tr>
                        <td>
                          <div class="alert alert-danger">Your level is empty</div>
                        </td>
                      </tr>
                    @endif
                    </tbody>
                  </table>

                  <div class="pull-right">
                    <button class="btn btn-block btn-primary" v-on:click="clickSubmitCourses($event)">
                      <i class="fa fa-check"></i>Submit
                    </button>
                  </div>
                </form>
                <div class="pull-right" style="margin-right: 20px;">
                  <button class="btn btn-block btn-success">
                    <i class="fa fa-plus"></i>Add Course
                  </button>
                </div>
              </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    window.Sms.department = {!! json_encode($departmentCourses) !!}
  </script>
@overwrite