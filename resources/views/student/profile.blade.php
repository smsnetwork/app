@extends('layouts.app')

@section('title', $user->name)

@section('content')
  <div class="pageheader">
    <h3><i class="fa fa-home"></i> User Profile </h3>
    <div class="breadcrumb-wrapper">
      <span class="label">You are here:</span>
      <ol class="breadcrumb">
        <li><a href="#"> Home </a></li>
        <li class="active"> User Profile</li>
      </ol>
    </div>
  </div>
  <!--Page content-->
  <!--===================================================-->
  <div id="page-content">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="panel">
          <div class="panel-body np">
            <img src="{{ asset('assets/img/user.png') }}" alt="{{ $user->name }}" class="img-responsive">
            <div class="text-center">
              <!-- panel body -->
              <h4 class="text-lg text-overflow mar-top">{{ $user->name }}</h4>
              <p class="text-sm">Student</p>
            </div>
          </div>
        </div>
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title"><i class="fa fa-users"></i> Online Friends </h4>
          </div>
          <div class="panel-body">
            <ul class="list-inline">
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="busy bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="busy bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="busy bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="busy bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="busy bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0)" class="pull-left avatar">
                  <img src="{{ asset('assets/img/user.png') }}" alt="" class="img-sm">
                  <i class="on bottom text-light"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"> </i> User Information</h3>
          </div>
          <div class="panel-body">
            <table class="table">
              <tbody>
              <tr>
                <td> Position</td>
                <td>Regular</td>
              </tr>
              <tr>
                <td> Last Logged In</td>
                <td>56 min</td>
              </tr>
              <tr>
                <td> Status</td>
                <td><span class="label label-sm label-info">Student</span></td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"> </i> Student Information</h3>
          </div>
          <div class="panel-body">
            <table class="table">
              <tbody>
              <tr>
                <td><i class="fa fa-envelope-o ph-5"></i></td>
                <td> Email</td>
                <td>{{ $user->email }} </td>
              </tr>
              <tr>
                <td><i class="fa fa-phone ph-5"></i></td>
                <td> Phone</td>
                <td> {{ $user->mobile }}</td>
              </tr>
              <tr>
                <td><i class="fa fa-skype ph-5"></i></td>
                <td> Level</td>
                <td> {{ ($user->current_level != 'null') ? $user->current_level->name : 'No Level Yet' }} </td>
              </tr>
              <tr>
                <td><i class="fa fa-external-link ph-5"></i></td>
                <td> Department</td>
                <td> {{ ($user->current_department != 'null') ? $user->current_department->name : 'No Department Yet' }} </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@overwrite