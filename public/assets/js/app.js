/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _noty = __webpack_require__(2);

	var _noty2 = _interopRequireDefault(_noty);

	var _course = __webpack_require__(39);

	var _course2 = _interopRequireDefault(_course);

	var _education = __webpack_require__(44);

	var _education2 = _interopRequireDefault(_education);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	Vue.use(_noty2.default);

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _defineProperties = __webpack_require__(3);

	var _defineProperties2 = _interopRequireDefault(_defineProperties);

	var _noty = __webpack_require__(37);

	var _noty2 = _interopRequireDefault(_noty);

	var _jquery = __webpack_require__(38);

	var _jquery2 = _interopRequireDefault(_jquery);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Notify = function Notify() {};

	Notify.prototype.show = function (type, message) {
	  var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

	  var payload = {
	    type: type,
	    text: message,
	    timeout: 2000,
	    layout: 'bottomRight'
	  };

	  return (0, _noty2.default)(_jquery2.default.extend(payload, options));
	};

	var helpers = ['alert', 'error', 'success', 'info', 'warning'];
	helpers.forEach(function (helper) {
	  Notify.prototype[helper] = function (message) {
	    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	    return this.show(helper, message, options);
	  };
	});

	Notify.install = function (Vue, options) {
	  Vue.notify = new Notify();
	  (0, _defineProperties2.default)(Vue.prototype, {
	    $notify: {
	      get: function get() {
	        return Vue.notify;
	      }
	    }
	  });
	};

	exports.default = Notify;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(4), __esModule: true };

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(5);
	var $Object = __webpack_require__(8).Object;
	module.exports = function defineProperties(T, D){
	  return $Object.defineProperties(T, D);
	};

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var $export = __webpack_require__(6);
	// 19.1.2.3 / 15.2.3.7 Object.defineProperties(O, Properties)
	$export($export.S + $export.F * !__webpack_require__(16), 'Object', {defineProperties: __webpack_require__(21)});

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	var global    = __webpack_require__(7)
	  , core      = __webpack_require__(8)
	  , ctx       = __webpack_require__(9)
	  , hide      = __webpack_require__(11)
	  , PROTOTYPE = 'prototype';

	var $export = function(type, name, source){
	  var IS_FORCED = type & $export.F
	    , IS_GLOBAL = type & $export.G
	    , IS_STATIC = type & $export.S
	    , IS_PROTO  = type & $export.P
	    , IS_BIND   = type & $export.B
	    , IS_WRAP   = type & $export.W
	    , exports   = IS_GLOBAL ? core : core[name] || (core[name] = {})
	    , expProto  = exports[PROTOTYPE]
	    , target    = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE]
	    , key, own, out;
	  if(IS_GLOBAL)source = name;
	  for(key in source){
	    // contains in native
	    own = !IS_FORCED && target && target[key] !== undefined;
	    if(own && key in exports)continue;
	    // export native or passed
	    out = own ? target[key] : source[key];
	    // prevent global pollution for namespaces
	    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
	    // bind timers to global for call from export context
	    : IS_BIND && own ? ctx(out, global)
	    // wrap global constructors for prevent change them in library
	    : IS_WRAP && target[key] == out ? (function(C){
	      var F = function(a, b, c){
	        if(this instanceof C){
	          switch(arguments.length){
	            case 0: return new C;
	            case 1: return new C(a);
	            case 2: return new C(a, b);
	          } return new C(a, b, c);
	        } return C.apply(this, arguments);
	      };
	      F[PROTOTYPE] = C[PROTOTYPE];
	      return F;
	    // make static versions for prototype methods
	    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
	    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
	    if(IS_PROTO){
	      (exports.virtual || (exports.virtual = {}))[key] = out;
	      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
	      if(type & $export.R && expProto && !expProto[key])hide(expProto, key, out);
	    }
	  }
	};
	// type bitmap
	$export.F = 1;   // forced
	$export.G = 2;   // global
	$export.S = 4;   // static
	$export.P = 8;   // proto
	$export.B = 16;  // bind
	$export.W = 32;  // wrap
	$export.U = 64;  // safe
	$export.R = 128; // real proto method for `library` 
	module.exports = $export;

/***/ },
/* 7 */
/***/ function(module, exports) {

	// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
	var global = module.exports = typeof window != 'undefined' && window.Math == Math
	  ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();
	if(typeof __g == 'number')__g = global; // eslint-disable-line no-undef

/***/ },
/* 8 */
/***/ function(module, exports) {

	var core = module.exports = {version: '2.4.0'};
	if(typeof __e == 'number')__e = core; // eslint-disable-line no-undef

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	// optional / simple context binding
	var aFunction = __webpack_require__(10);
	module.exports = function(fn, that, length){
	  aFunction(fn);
	  if(that === undefined)return fn;
	  switch(length){
	    case 1: return function(a){
	      return fn.call(that, a);
	    };
	    case 2: return function(a, b){
	      return fn.call(that, a, b);
	    };
	    case 3: return function(a, b, c){
	      return fn.call(that, a, b, c);
	    };
	  }
	  return function(/* ...args */){
	    return fn.apply(that, arguments);
	  };
	};

/***/ },
/* 10 */
/***/ function(module, exports) {

	module.exports = function(it){
	  if(typeof it != 'function')throw TypeError(it + ' is not a function!');
	  return it;
	};

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var dP         = __webpack_require__(12)
	  , createDesc = __webpack_require__(20);
	module.exports = __webpack_require__(16) ? function(object, key, value){
	  return dP.f(object, key, createDesc(1, value));
	} : function(object, key, value){
	  object[key] = value;
	  return object;
	};

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var anObject       = __webpack_require__(13)
	  , IE8_DOM_DEFINE = __webpack_require__(15)
	  , toPrimitive    = __webpack_require__(19)
	  , dP             = Object.defineProperty;

	exports.f = __webpack_require__(16) ? Object.defineProperty : function defineProperty(O, P, Attributes){
	  anObject(O);
	  P = toPrimitive(P, true);
	  anObject(Attributes);
	  if(IE8_DOM_DEFINE)try {
	    return dP(O, P, Attributes);
	  } catch(e){ /* empty */ }
	  if('get' in Attributes || 'set' in Attributes)throw TypeError('Accessors not supported!');
	  if('value' in Attributes)O[P] = Attributes.value;
	  return O;
	};

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(14);
	module.exports = function(it){
	  if(!isObject(it))throw TypeError(it + ' is not an object!');
	  return it;
	};

/***/ },
/* 14 */
/***/ function(module, exports) {

	module.exports = function(it){
	  return typeof it === 'object' ? it !== null : typeof it === 'function';
	};

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = !__webpack_require__(16) && !__webpack_require__(17)(function(){
	  return Object.defineProperty(__webpack_require__(18)('div'), 'a', {get: function(){ return 7; }}).a != 7;
	});

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	// Thank's IE8 for his funny defineProperty
	module.exports = !__webpack_require__(17)(function(){
	  return Object.defineProperty({}, 'a', {get: function(){ return 7; }}).a != 7;
	});

/***/ },
/* 17 */
/***/ function(module, exports) {

	module.exports = function(exec){
	  try {
	    return !!exec();
	  } catch(e){
	    return true;
	  }
	};

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(14)
	  , document = __webpack_require__(7).document
	  // in old IE typeof document.createElement is 'object'
	  , is = isObject(document) && isObject(document.createElement);
	module.exports = function(it){
	  return is ? document.createElement(it) : {};
	};

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	// 7.1.1 ToPrimitive(input [, PreferredType])
	var isObject = __webpack_require__(14);
	// instead of the ES6 spec version, we didn't implement @@toPrimitive case
	// and the second argument - flag - preferred type is a string
	module.exports = function(it, S){
	  if(!isObject(it))return it;
	  var fn, val;
	  if(S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it)))return val;
	  if(typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it)))return val;
	  if(!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it)))return val;
	  throw TypeError("Can't convert object to primitive value");
	};

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = function(bitmap, value){
	  return {
	    enumerable  : !(bitmap & 1),
	    configurable: !(bitmap & 2),
	    writable    : !(bitmap & 4),
	    value       : value
	  };
	};

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var dP       = __webpack_require__(12)
	  , anObject = __webpack_require__(13)
	  , getKeys  = __webpack_require__(22);

	module.exports = __webpack_require__(16) ? Object.defineProperties : function defineProperties(O, Properties){
	  anObject(O);
	  var keys   = getKeys(Properties)
	    , length = keys.length
	    , i = 0
	    , P;
	  while(length > i)dP.f(O, P = keys[i++], Properties[P]);
	  return O;
	};

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	// 19.1.2.14 / 15.2.3.14 Object.keys(O)
	var $keys       = __webpack_require__(23)
	  , enumBugKeys = __webpack_require__(36);

	module.exports = Object.keys || function keys(O){
	  return $keys(O, enumBugKeys);
	};

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	var has          = __webpack_require__(24)
	  , toIObject    = __webpack_require__(25)
	  , arrayIndexOf = __webpack_require__(29)(false)
	  , IE_PROTO     = __webpack_require__(33)('IE_PROTO');

	module.exports = function(object, names){
	  var O      = toIObject(object)
	    , i      = 0
	    , result = []
	    , key;
	  for(key in O)if(key != IE_PROTO)has(O, key) && result.push(key);
	  // Don't enum bug & hidden keys
	  while(names.length > i)if(has(O, key = names[i++])){
	    ~arrayIndexOf(result, key) || result.push(key);
	  }
	  return result;
	};

/***/ },
/* 24 */
/***/ function(module, exports) {

	var hasOwnProperty = {}.hasOwnProperty;
	module.exports = function(it, key){
	  return hasOwnProperty.call(it, key);
	};

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	// to indexed object, toObject with fallback for non-array-like ES3 strings
	var IObject = __webpack_require__(26)
	  , defined = __webpack_require__(28);
	module.exports = function(it){
	  return IObject(defined(it));
	};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	// fallback for non-array-like ES3 and non-enumerable old V8 strings
	var cof = __webpack_require__(27);
	module.exports = Object('z').propertyIsEnumerable(0) ? Object : function(it){
	  return cof(it) == 'String' ? it.split('') : Object(it);
	};

/***/ },
/* 27 */
/***/ function(module, exports) {

	var toString = {}.toString;

	module.exports = function(it){
	  return toString.call(it).slice(8, -1);
	};

/***/ },
/* 28 */
/***/ function(module, exports) {

	// 7.2.1 RequireObjectCoercible(argument)
	module.exports = function(it){
	  if(it == undefined)throw TypeError("Can't call method on  " + it);
	  return it;
	};

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	// false -> Array#indexOf
	// true  -> Array#includes
	var toIObject = __webpack_require__(25)
	  , toLength  = __webpack_require__(30)
	  , toIndex   = __webpack_require__(32);
	module.exports = function(IS_INCLUDES){
	  return function($this, el, fromIndex){
	    var O      = toIObject($this)
	      , length = toLength(O.length)
	      , index  = toIndex(fromIndex, length)
	      , value;
	    // Array#includes uses SameValueZero equality algorithm
	    if(IS_INCLUDES && el != el)while(length > index){
	      value = O[index++];
	      if(value != value)return true;
	    // Array#toIndex ignores holes, Array#includes - not
	    } else for(;length > index; index++)if(IS_INCLUDES || index in O){
	      if(O[index] === el)return IS_INCLUDES || index || 0;
	    } return !IS_INCLUDES && -1;
	  };
	};

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	// 7.1.15 ToLength
	var toInteger = __webpack_require__(31)
	  , min       = Math.min;
	module.exports = function(it){
	  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
	};

/***/ },
/* 31 */
/***/ function(module, exports) {

	// 7.1.4 ToInteger
	var ceil  = Math.ceil
	  , floor = Math.floor;
	module.exports = function(it){
	  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
	};

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var toInteger = __webpack_require__(31)
	  , max       = Math.max
	  , min       = Math.min;
	module.exports = function(index, length){
	  index = toInteger(index);
	  return index < 0 ? max(index + length, 0) : min(index, length);
	};

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var shared = __webpack_require__(34)('keys')
	  , uid    = __webpack_require__(35);
	module.exports = function(key){
	  return shared[key] || (shared[key] = uid(key));
	};

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var global = __webpack_require__(7)
	  , SHARED = '__core-js_shared__'
	  , store  = global[SHARED] || (global[SHARED] = {});
	module.exports = function(key){
	  return store[key] || (store[key] = {});
	};

/***/ },
/* 35 */
/***/ function(module, exports) {

	var id = 0
	  , px = Math.random();
	module.exports = function(key){
	  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
	};

/***/ },
/* 36 */
/***/ function(module, exports) {

	// IE 8- don't enum bug keys
	module.exports = (
	  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
	).split(',');

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!function(root, factory) {
		 if (true) {
			 !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(38)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		 } else if (typeof exports === 'object') {
			 module.exports = factory(require('jquery'));
		 } else {
			 factory(root.jQuery);
		 }
	}(this, function($) {

	/*!
	 @package noty - jQuery Notification Plugin
	 @version version: 2.3.8
	 @contributors https://github.com/needim/noty/graphs/contributors

	 @documentation Examples and Documentation - http://needim.github.com/noty/

	 @license Licensed under the MIT licenses: http://www.opensource.org/licenses/mit-license.php
	 */

	    if(typeof Object.create !== 'function') {
	        Object.create = function(o) {
	            function F() {
	            }

	            F.prototype = o;
	            return new F();
	        };
	    }

	    var NotyObject = {

	        init: function(options) {

	            // Mix in the passed in options with the default options
	            this.options = $.extend({}, $.noty.defaults, options);

	            this.options.layout = (this.options.custom) ? $.noty.layouts['inline'] : $.noty.layouts[this.options.layout];

	            if($.noty.themes[this.options.theme])
	                this.options.theme = $.noty.themes[this.options.theme];
	            else
	                this.options.themeClassName = this.options.theme;

	            this.options = $.extend({}, this.options, this.options.layout.options);
	            this.options.id = 'noty_' + (new Date().getTime() * Math.floor(Math.random() * 1000000));

	            // Build the noty dom initial structure
	            this._build();

	            // return this so we can chain/use the bridge with less code.
	            return this;
	        }, // end init

	        _build: function() {

	            // Generating noty bar
	            var $bar = $('<div class="noty_bar noty_type_' + this.options.type + '"></div>').attr('id', this.options.id);
	            $bar.append(this.options.template).find('.noty_text').html(this.options.text);

	            this.$bar = (this.options.layout.parent.object !== null) ? $(this.options.layout.parent.object).css(this.options.layout.parent.css).append($bar) : $bar;

	            if(this.options.themeClassName)
	                this.$bar.addClass(this.options.themeClassName).addClass('noty_container_type_' + this.options.type);

	            // Set buttons if available
	            if(this.options.buttons) {

	                // If we have button disable closeWith & timeout options
	                this.options.closeWith = [];
	                this.options.timeout = false;

	                var $buttons = $('<div/>').addClass('noty_buttons');

	                (this.options.layout.parent.object !== null) ? this.$bar.find('.noty_bar').append($buttons) : this.$bar.append($buttons);

	                var self = this;

	                $.each(this.options.buttons, function(i, button) {
	                    var $button = $('<button/>').addClass((button.addClass) ? button.addClass : 'gray').html(button.text).attr('id', button.id ? button.id : 'button-' + i)
	                        .attr('title', button.title)
	                        .appendTo(self.$bar.find('.noty_buttons'))
	                        .on('click', function(event) {
	                            if($.isFunction(button.onClick)) {
	                                button.onClick.call($button, self, event);
	                            }
	                        });
	                });
	            }

	            // For easy access
	            this.$message = this.$bar.find('.noty_message');
	            this.$closeButton = this.$bar.find('.noty_close');
	            this.$buttons = this.$bar.find('.noty_buttons');

	            $.noty.store[this.options.id] = this; // store noty for api

	        }, // end _build

	        show: function() {

	            var self = this;

	            (self.options.custom) ? self.options.custom.find(self.options.layout.container.selector).append(self.$bar) : $(self.options.layout.container.selector).append(self.$bar);

	            if(self.options.theme && self.options.theme.style)
	                self.options.theme.style.apply(self);

	            ($.type(self.options.layout.css) === 'function') ? this.options.layout.css.apply(self.$bar) : self.$bar.css(this.options.layout.css || {});

	            self.$bar.addClass(self.options.layout.addClass);

	            self.options.layout.container.style.apply($(self.options.layout.container.selector), [self.options.within]);

	            self.showing = true;

	            if(self.options.theme && self.options.theme.style)
	                self.options.theme.callback.onShow.apply(this);

	            if($.inArray('click', self.options.closeWith) > -1)
	                self.$bar.css('cursor', 'pointer').one('click', function(evt) {
	                    self.stopPropagation(evt);
	                    if(self.options.callback.onCloseClick) {
	                        self.options.callback.onCloseClick.apply(self);
	                    }
	                    self.close();
	                });

	            if($.inArray('hover', self.options.closeWith) > -1)
	                self.$bar.one('mouseenter', function() {
	                    self.close();
	                });

	            if($.inArray('button', self.options.closeWith) > -1)
	                self.$closeButton.one('click', function(evt) {
	                    self.stopPropagation(evt);
	                    self.close();
	                });

	            if($.inArray('button', self.options.closeWith) == -1)
	                self.$closeButton.remove();

	            if(self.options.callback.onShow)
	                self.options.callback.onShow.apply(self);

	            if (typeof self.options.animation.open == 'string') {
	                self.$bar.css('height', self.$bar.innerHeight());
	                self.$bar.on('click',function(e){
	                    self.wasClicked = true;
	                });
	                self.$bar.show().addClass(self.options.animation.open).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
	                    if(self.options.callback.afterShow) self.options.callback.afterShow.apply(self);
	                    self.showing = false;
	                    self.shown = true;
	                    if(self.hasOwnProperty('wasClicked')){
	                        self.$bar.off('click',function(e){
	                            self.wasClicked = true;
	                        });
	                        self.close();
	                    }
	                });

	            } else {
	                self.$bar.animate(
	                    self.options.animation.open,
	                    self.options.animation.speed,
	                    self.options.animation.easing,
	                    function() {
	                        if(self.options.callback.afterShow) self.options.callback.afterShow.apply(self);
	                        self.showing = false;
	                        self.shown = true;
	                    });
	            }

	            // If noty is have a timeout option
	            if(self.options.timeout)
	                self.$bar.delay(self.options.timeout).promise().done(function() {
	                    self.close();
	                });

	            return this;

	        }, // end show

	        close: function() {

	            if(this.closed) return;
	            if(this.$bar && this.$bar.hasClass('i-am-closing-now')) return;

	            var self = this;

	            if(this.showing) {
	                self.$bar.queue(
	                    function() {
	                        self.close.apply(self);
	                    }
	                );
	                return;
	            }

	            if(!this.shown && !this.showing) { // If we are still waiting in the queue just delete from queue
	                var queue = [];
	                $.each($.noty.queue, function(i, n) {
	                    if(n.options.id != self.options.id) {
	                        queue.push(n);
	                    }
	                });
	                $.noty.queue = queue;
	                return;
	            }

	            self.$bar.addClass('i-am-closing-now');

	            if(self.options.callback.onClose) {
	                self.options.callback.onClose.apply(self);
	            }

	            if (typeof self.options.animation.close == 'string') {
	                self.$bar.addClass(self.options.animation.close).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
	                    if(self.options.callback.afterClose) self.options.callback.afterClose.apply(self);
	                    self.closeCleanUp();
	                });
	            } else {
	                self.$bar.clearQueue().stop().animate(
	                    self.options.animation.close,
	                    self.options.animation.speed,
	                    self.options.animation.easing,
	                    function() {
	                        if(self.options.callback.afterClose) self.options.callback.afterClose.apply(self);
	                    })
	                    .promise().done(function() {
	                        self.closeCleanUp();
	                    });
	            }

	        }, // end close

	        closeCleanUp: function() {

	            var self = this;

	            // Modal Cleaning
	            if(self.options.modal) {
	                $.notyRenderer.setModalCount(-1);
	                if($.notyRenderer.getModalCount() == 0) $('.noty_modal').fadeOut(self.options.animation.fadeSpeed, function() {
	                    $(this).remove();
	                });
	            }

	            // Layout Cleaning
	            $.notyRenderer.setLayoutCountFor(self, -1);
	            if($.notyRenderer.getLayoutCountFor(self) == 0) $(self.options.layout.container.selector).remove();

	            // Make sure self.$bar has not been removed before attempting to remove it
	            if(typeof self.$bar !== 'undefined' && self.$bar !== null) {

	                if (typeof self.options.animation.close == 'string') {
	                    self.$bar.css('transition', 'all 100ms ease').css('border', 0).css('margin', 0).height(0);
	                    self.$bar.one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
	                        self.$bar.remove();
	                        self.$bar = null;
	                        self.closed = true;

	                        if(self.options.theme.callback && self.options.theme.callback.onClose) {
	                            self.options.theme.callback.onClose.apply(self);
	                        }
	                    });
	                } else {
	                    self.$bar.remove();
	                    self.$bar = null;
	                    self.closed = true;
	                }
	            }

	            delete $.noty.store[self.options.id]; // deleting noty from store

	            if(self.options.theme.callback && self.options.theme.callback.onClose) {
	                self.options.theme.callback.onClose.apply(self);
	            }

	            if(!self.options.dismissQueue) {
	                // Queue render
	                $.noty.ontap = true;

	                $.notyRenderer.render();
	            }

	            if(self.options.maxVisible > 0 && self.options.dismissQueue) {
	                $.notyRenderer.render();
	            }

	        }, // end close clean up

	        setText: function(text) {
	            if(!this.closed) {
	                this.options.text = text;
	                this.$bar.find('.noty_text').html(text);
	            }
	            return this;
	        },

	        setType: function(type) {
	            if(!this.closed) {
	                this.options.type = type;
	                this.options.theme.style.apply(this);
	                this.options.theme.callback.onShow.apply(this);
	            }
	            return this;
	        },

	        setTimeout: function(time) {
	            if(!this.closed) {
	                var self = this;
	                this.options.timeout = time;
	                self.$bar.delay(self.options.timeout).promise().done(function() {
	                    self.close();
	                });
	            }
	            return this;
	        },

	        stopPropagation: function(evt) {
	            evt = evt || window.event;
	            if(typeof evt.stopPropagation !== "undefined") {
	                evt.stopPropagation();
	            }
	            else {
	                evt.cancelBubble = true;
	            }
	        },

	        closed : false,
	        showing: false,
	        shown  : false

	    }; // end NotyObject

	    $.notyRenderer = {};

	    $.notyRenderer.init = function(options) {

	        // Renderer creates a new noty
	        var notification = Object.create(NotyObject).init(options);

	        if(notification.options.killer)
	            $.noty.closeAll();

	        (notification.options.force) ? $.noty.queue.unshift(notification) : $.noty.queue.push(notification);

	        $.notyRenderer.render();

	        return ($.noty.returns == 'object') ? notification : notification.options.id;
	    };

	    $.notyRenderer.render = function() {

	        var instance = $.noty.queue[0];

	        if($.type(instance) === 'object') {
	            if(instance.options.dismissQueue) {
	                if(instance.options.maxVisible > 0) {
	                    if($(instance.options.layout.container.selector + ' > li').length < instance.options.maxVisible) {
	                        $.notyRenderer.show($.noty.queue.shift());
	                    }
	                    else {

	                    }
	                }
	                else {
	                    $.notyRenderer.show($.noty.queue.shift());
	                }
	            }
	            else {
	                if($.noty.ontap) {
	                    $.notyRenderer.show($.noty.queue.shift());
	                    $.noty.ontap = false;
	                }
	            }
	        }
	        else {
	            $.noty.ontap = true; // Queue is over
	        }

	    };

	    $.notyRenderer.show = function(notification) {

	        if(notification.options.modal) {
	            $.notyRenderer.createModalFor(notification);
	            $.notyRenderer.setModalCount(+1);
	        }

	        // Where is the container?
	        if(notification.options.custom) {
	            if(notification.options.custom.find(notification.options.layout.container.selector).length == 0) {
	                notification.options.custom.append($(notification.options.layout.container.object).addClass('i-am-new'));
	            }
	            else {
	                notification.options.custom.find(notification.options.layout.container.selector).removeClass('i-am-new');
	            }
	        }
	        else {
	            if($(notification.options.layout.container.selector).length == 0) {
	                $('body').append($(notification.options.layout.container.object).addClass('i-am-new'));
	            }
	            else {
	                $(notification.options.layout.container.selector).removeClass('i-am-new');
	            }
	        }

	        $.notyRenderer.setLayoutCountFor(notification, +1);

	        notification.show();
	    };

	    $.notyRenderer.createModalFor = function(notification) {
	        if($('.noty_modal').length == 0) {
	            var modal = $('<div/>').addClass('noty_modal').addClass(notification.options.theme).data('noty_modal_count', 0);

	            if(notification.options.theme.modal && notification.options.theme.modal.css)
	                modal.css(notification.options.theme.modal.css);

	            modal.prependTo($('body')).fadeIn(notification.options.animation.fadeSpeed);

	            if($.inArray('backdrop', notification.options.closeWith) > -1)
	                modal.on('click', function(e) {
	                    $.noty.closeAll();
	                });
	        }
	    };

	    $.notyRenderer.getLayoutCountFor = function(notification) {
	        return $(notification.options.layout.container.selector).data('noty_layout_count') || 0;
	    };

	    $.notyRenderer.setLayoutCountFor = function(notification, arg) {
	        return $(notification.options.layout.container.selector).data('noty_layout_count', $.notyRenderer.getLayoutCountFor(notification) + arg);
	    };

	    $.notyRenderer.getModalCount = function() {
	        return $('.noty_modal').data('noty_modal_count') || 0;
	    };

	    $.notyRenderer.setModalCount = function(arg) {
	        return $('.noty_modal').data('noty_modal_count', $.notyRenderer.getModalCount() + arg);
	    };

	    // This is for custom container
	    $.fn.noty = function(options) {
	        options.custom = $(this);
	        return $.notyRenderer.init(options);
	    };

	    $.noty = {};
	    $.noty.queue = [];
	    $.noty.ontap = true;
	    $.noty.layouts = {};
	    $.noty.themes = {};
	    $.noty.returns = 'object';
	    $.noty.store = {};

	    $.noty.get = function(id) {
	        return $.noty.store.hasOwnProperty(id) ? $.noty.store[id] : false;
	    };

	    $.noty.close = function(id) {
	        return $.noty.get(id) ? $.noty.get(id).close() : false;
	    };

	    $.noty.setText = function(id, text) {
	        return $.noty.get(id) ? $.noty.get(id).setText(text) : false;
	    };

	    $.noty.setType = function(id, type) {
	        return $.noty.get(id) ? $.noty.get(id).setType(type) : false;
	    };

	    $.noty.clearQueue = function() {
	        $.noty.queue = [];
	    };

	    $.noty.closeAll = function() {
	        $.noty.clearQueue();
	        $.each($.noty.store, function(id, noty) {
	            noty.close();
	        });
	    };

	    var windowAlert = window.alert;

	    $.noty.consumeAlert = function(options) {
	        window.alert = function(text) {
	            if(options)
	                options.text = text;
	            else
	                options = {text: text};

	            $.notyRenderer.init(options);
	        };
	    };

	    $.noty.stopConsumeAlert = function() {
	        window.alert = windowAlert;
	    };

	    $.noty.defaults = {
	        layout      : 'top',
	        theme       : 'defaultTheme',
	        type        : 'alert',
	        text        : '',
	        dismissQueue: true,
	        template    : '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
	        animation   : {
	            open  : {height: 'toggle'},
	            close : {height: 'toggle'},
	            easing: 'swing',
	            speed : 500,
	            fadeSpeed: 'fast',
	        },
	        timeout     : false,
	        force       : false,
	        modal       : false,
	        maxVisible  : 5,
	        killer      : false,
	        closeWith   : ['click'],
	        callback    : {
	            onShow      : function() {
	            },
	            afterShow   : function() {
	            },
	            onClose     : function() {
	            },
	            afterClose  : function() {
	            },
	            onCloseClick: function() {
	            }
	        },
	        buttons     : false
	    };

	    $(window).on('resize', function() {
	        $.each($.noty.layouts, function(index, layout) {
	            layout.container.style.apply($(layout.container.selector));
	        });
	    });

	    // Helpers
	    window.noty = function noty(options) {
	        return $.notyRenderer.init(options);
	    };

	$.noty.layouts.bottom = {
	    name     : 'bottom',
	    options  : {},
	    container: {
	        object  : '<ul id="noty_bottom_layout_container" />',
	        selector: 'ul#noty_bottom_layout_container',
	        style   : function() {
	            $(this).css({
	                bottom       : 0,
	                left         : '5%',
	                position     : 'fixed',
	                width        : '90%',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 9999999
	            });
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none'
	    },
	    addClass : ''
	};

	$.noty.layouts.bottomCenter = {
	    name     : 'bottomCenter',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_bottomCenter_layout_container" />',
	        selector: 'ul#noty_bottomCenter_layout_container',
	        style   : function() {
	            $(this).css({
	                bottom       : 20,
	                left         : 0,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            $(this).css({
	                left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px'
	            });
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};


	$.noty.layouts.bottomLeft = {
	    name     : 'bottomLeft',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_bottomLeft_layout_container" />',
	        selector: 'ul#noty_bottomLeft_layout_container',
	        style   : function() {
	            $(this).css({
	                bottom       : 20,
	                left         : 20,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            if(window.innerWidth < 600) {
	                $(this).css({
	                    left: 5
	                });
	            }
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};
	$.noty.layouts.bottomRight = {
	    name     : 'bottomRight',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_bottomRight_layout_container" />',
	        selector: 'ul#noty_bottomRight_layout_container',
	        style   : function() {
	            $(this).css({
	                bottom       : 20,
	                right        : 20,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            if(window.innerWidth < 600) {
	                $(this).css({
	                    right: 5
	                });
	            }
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};
	$.noty.layouts.center = {
	    name     : 'center',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_center_layout_container" />',
	        selector: 'ul#noty_center_layout_container',
	        style   : function() {
	            $(this).css({
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            // getting hidden height
	            var dupe = $(this).clone().css({visibility: "hidden", display: "block", position: "absolute", top: 0, left: 0}).attr('id', 'dupe');
	            $("body").append(dupe);
	            dupe.find('.i-am-closing-now').remove();
	            dupe.find('li').css('display', 'block');
	            var actual_height = dupe.height();
	            dupe.remove();

	            if($(this).hasClass('i-am-new')) {
	                $(this).css({
	                    left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px',
	                    top : ($(window).height() - actual_height) / 2 + 'px'
	                });
	            }
	            else {
	                $(this).animate({
	                    left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px',
	                    top : ($(window).height() - actual_height) / 2 + 'px'
	                }, 500);
	            }

	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};
	$.noty.layouts.centerLeft = {
	    name     : 'centerLeft',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_centerLeft_layout_container" />',
	        selector: 'ul#noty_centerLeft_layout_container',
	        style   : function() {
	            $(this).css({
	                left         : 20,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            // getting hidden height
	            var dupe = $(this).clone().css({visibility: "hidden", display: "block", position: "absolute", top: 0, left: 0}).attr('id', 'dupe');
	            $("body").append(dupe);
	            dupe.find('.i-am-closing-now').remove();
	            dupe.find('li').css('display', 'block');
	            var actual_height = dupe.height();
	            dupe.remove();

	            if($(this).hasClass('i-am-new')) {
	                $(this).css({
	                    top: ($(window).height() - actual_height) / 2 + 'px'
	                });
	            }
	            else {
	                $(this).animate({
	                    top: ($(window).height() - actual_height) / 2 + 'px'
	                }, 500);
	            }

	            if(window.innerWidth < 600) {
	                $(this).css({
	                    left: 5
	                });
	            }

	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};

	$.noty.layouts.centerRight = {
	    name     : 'centerRight',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_centerRight_layout_container" />',
	        selector: 'ul#noty_centerRight_layout_container',
	        style   : function() {
	            $(this).css({
	                right        : 20,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            // getting hidden height
	            var dupe = $(this).clone().css({visibility: "hidden", display: "block", position: "absolute", top: 0, left: 0}).attr('id', 'dupe');
	            $("body").append(dupe);
	            dupe.find('.i-am-closing-now').remove();
	            dupe.find('li').css('display', 'block');
	            var actual_height = dupe.height();
	            dupe.remove();

	            if($(this).hasClass('i-am-new')) {
	                $(this).css({
	                    top: ($(window).height() - actual_height) / 2 + 'px'
	                });
	            }
	            else {
	                $(this).animate({
	                    top: ($(window).height() - actual_height) / 2 + 'px'
	                }, 500);
	            }

	            if(window.innerWidth < 600) {
	                $(this).css({
	                    right: 5
	                });
	            }

	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};
	$.noty.layouts.inline = {
	    name     : 'inline',
	    options  : {},
	    container: {
	        object  : '<ul class="noty_inline_layout_container" />',
	        selector: 'ul.noty_inline_layout_container',
	        style   : function() {
	            $(this).css({
	                width        : '100%',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 9999999
	            });
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none'
	    },
	    addClass : ''
	};
	$.noty.layouts.top = {
	    name     : 'top',
	    options  : {},
	    container: {
	        object  : '<ul id="noty_top_layout_container" />',
	        selector: 'ul#noty_top_layout_container',
	        style   : function() {
	            $(this).css({
	                top          : 0,
	                left         : '5%',
	                position     : 'fixed',
	                width        : '90%',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 9999999
	            });
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none'
	    },
	    addClass : ''
	};
	$.noty.layouts.topCenter = {
	    name     : 'topCenter',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_topCenter_layout_container" />',
	        selector: 'ul#noty_topCenter_layout_container',
	        style   : function() {
	            $(this).css({
	                top          : 20,
	                left         : 0,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            $(this).css({
	                left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px'
	            });
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};

	$.noty.layouts.topLeft = {
	    name     : 'topLeft',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_topLeft_layout_container" />',
	        selector: 'ul#noty_topLeft_layout_container',
	        style   : function() {
	            $(this).css({
	                top          : 20,
	                left         : 20,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            if(window.innerWidth < 600) {
	                $(this).css({
	                    left: 5
	                });
	            }
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};
	$.noty.layouts.topRight = {
	    name     : 'topRight',
	    options  : { // overrides options

	    },
	    container: {
	        object  : '<ul id="noty_topRight_layout_container" />',
	        selector: 'ul#noty_topRight_layout_container',
	        style   : function() {
	            $(this).css({
	                top          : 20,
	                right        : 20,
	                position     : 'fixed',
	                width        : '310px',
	                height       : 'auto',
	                margin       : 0,
	                padding      : 0,
	                listStyleType: 'none',
	                zIndex       : 10000000
	            });

	            if(window.innerWidth < 600) {
	                $(this).css({
	                    right: 5
	                });
	            }
	        }
	    },
	    parent   : {
	        object  : '<li />',
	        selector: 'li',
	        css     : {}
	    },
	    css      : {
	        display: 'none',
	        width  : '310px'
	    },
	    addClass : ''
	};
	$.noty.themes.bootstrapTheme = {
	    name: 'bootstrapTheme',
	    modal: {
	        css: {
	            position: 'fixed',
	            width: '100%',
	            height: '100%',
	            backgroundColor: '#000',
	            zIndex: 10000,
	            opacity: 0.6,
	            display: 'none',
	            left: 0,
	            top: 0
	        }
	    },
	    style: function() {

	        var containerSelector = this.options.layout.container.selector;
	        $(containerSelector).addClass('list-group');

	        this.$closeButton.append('<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>');
	        this.$closeButton.addClass('close');

	        this.$bar.addClass( "list-group-item" ).css('padding', '0px');

	        switch (this.options.type) {
	            case 'alert': case 'notification':
	                this.$bar.addClass( "list-group-item-info" );
	                break;
	            case 'warning':
	                this.$bar.addClass( "list-group-item-warning" );
	                break;
	            case 'error':
	                this.$bar.addClass( "list-group-item-danger" );
	                break;
	            case 'information':
	                this.$bar.addClass("list-group-item-info");
	                break;
	            case 'success':
	                this.$bar.addClass( "list-group-item-success" );
	                break;
	        }

	        this.$message.css({
	            fontSize: '13px',
	            lineHeight: '16px',
	            textAlign: 'center',
	            padding: '8px 10px 9px',
	            width: 'auto',
	            position: 'relative'
	        });
	    },
	    callback: {
	        onShow: function() {  },
	        onClose: function() {  }
	    }
	};


	$.noty.themes.defaultTheme = {
	    name    : 'defaultTheme',
	    helpers : {
	        borderFix: function() {
	            if(this.options.dismissQueue) {
	                var selector = this.options.layout.container.selector + ' ' + this.options.layout.parent.selector;
	                switch(this.options.layout.name) {
	                    case 'top':
	                        $(selector).css({borderRadius: '0px 0px 0px 0px'});
	                        $(selector).last().css({borderRadius: '0px 0px 5px 5px'});
	                        break;
	                    case 'topCenter':
	                    case 'topLeft':
	                    case 'topRight':
	                    case 'bottomCenter':
	                    case 'bottomLeft':
	                    case 'bottomRight':
	                    case 'center':
	                    case 'centerLeft':
	                    case 'centerRight':
	                    case 'inline':
	                        $(selector).css({borderRadius: '0px 0px 0px 0px'});
	                        $(selector).first().css({'border-top-left-radius': '5px', 'border-top-right-radius': '5px'});
	                        $(selector).last().css({'border-bottom-left-radius': '5px', 'border-bottom-right-radius': '5px'});
	                        break;
	                    case 'bottom':
	                        $(selector).css({borderRadius: '0px 0px 0px 0px'});
	                        $(selector).first().css({borderRadius: '5px 5px 0px 0px'});
	                        break;
	                    default:
	                        break;
	                }
	            }
	        }
	    },
	    modal   : {
	        css: {
	            position       : 'fixed',
	            width          : '100%',
	            height         : '100%',
	            backgroundColor: '#000',
	            zIndex         : 10000,
	            opacity        : 0.6,
	            display        : 'none',
	            left           : 0,
	            top            : 0
	        }
	    },
	    style   : function() {

	        this.$bar.css({
	            overflow  : 'hidden',
	            background: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=') repeat-x scroll left top #fff"
	        });

	        this.$message.css({
	            fontSize  : '13px',
	            lineHeight: '16px',
	            textAlign : 'center',
	            padding   : '8px 10px 9px',
	            width     : 'auto',
	            position  : 'relative'
	        });

	        this.$closeButton.css({
	            position  : 'absolute',
	            top       : 4, right: 4,
	            width     : 10, height: 10,
	            background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAxUlEQVR4AR3MPUoDURSA0e++uSkkOxC3IAOWNtaCIDaChfgXBMEZbQRByxCwk+BasgQRZLSYoLgDQbARxry8nyumPcVRKDfd0Aa8AsgDv1zp6pYd5jWOwhvebRTbzNNEw5BSsIpsj/kurQBnmk7sIFcCF5yyZPDRG6trQhujXYosaFoc+2f1MJ89uc76IND6F9BvlXUdpb6xwD2+4q3me3bysiHvtLYrUJto7PD/ve7LNHxSg/woN2kSz4txasBdhyiz3ugPGetTjm3XRokAAAAASUVORK5CYII=)",
	            display   : 'none',
	            cursor    : 'pointer'
	        });

	        this.$buttons.css({
	            padding        : 5,
	            textAlign      : 'right',
	            borderTop      : '1px solid #ccc',
	            backgroundColor: '#fff'
	        });

	        this.$buttons.find('button').css({
	            marginLeft: 5
	        });

	        this.$buttons.find('button:first').css({
	            marginLeft: 0
	        });

	        this.$bar.on({
	            mouseenter: function() {
	                $(this).find('.noty_close').stop().fadeTo('normal', 1);
	            },
	            mouseleave: function() {
	                $(this).find('.noty_close').stop().fadeTo('normal', 0);
	            }
	        });

	        switch(this.options.layout.name) {
	            case 'top':
	                this.$bar.css({
	                    borderRadius: '0px 0px 5px 5px',
	                    borderBottom: '2px solid #eee',
	                    borderLeft  : '2px solid #eee',
	                    borderRight : '2px solid #eee',
	                    boxShadow   : "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                break;
	            case 'topCenter':
	            case 'center':
	            case 'bottomCenter':
	            case 'inline':
	                this.$bar.css({
	                    borderRadius: '5px',
	                    border      : '1px solid #eee',
	                    boxShadow   : "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                this.$message.css({fontSize: '13px', textAlign: 'center'});
	                break;
	            case 'topLeft':
	            case 'topRight':
	            case 'bottomLeft':
	            case 'bottomRight':
	            case 'centerLeft':
	            case 'centerRight':
	                this.$bar.css({
	                    borderRadius: '5px',
	                    border      : '1px solid #eee',
	                    boxShadow   : "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                this.$message.css({fontSize: '13px', textAlign: 'left'});
	                break;
	            case 'bottom':
	                this.$bar.css({
	                    borderRadius: '5px 5px 0px 0px',
	                    borderTop   : '2px solid #eee',
	                    borderLeft  : '2px solid #eee',
	                    borderRight : '2px solid #eee',
	                    boxShadow   : "0 -2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                break;
	            default:
	                this.$bar.css({
	                    border   : '2px solid #eee',
	                    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                break;
	        }

	        switch(this.options.type) {
	            case 'alert':
	            case 'notification':
	                this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'});
	                break;
	            case 'warning':
	                this.$bar.css({backgroundColor: '#FFEAA8', borderColor: '#FFC237', color: '#826200'});
	                this.$buttons.css({borderTop: '1px solid #FFC237'});
	                break;
	            case 'error':
	                this.$bar.css({backgroundColor: 'red', borderColor: 'darkred', color: '#FFF'});
	                this.$message.css({fontWeight: 'bold'});
	                this.$buttons.css({borderTop: '1px solid darkred'});
	                break;
	            case 'information':
	                this.$bar.css({backgroundColor: '#57B7E2', borderColor: '#0B90C4', color: '#FFF'});
	                this.$buttons.css({borderTop: '1px solid #0B90C4'});
	                break;
	            case 'success':
	                this.$bar.css({backgroundColor: 'lightgreen', borderColor: '#50C24E', color: 'darkgreen'});
	                this.$buttons.css({borderTop: '1px solid #50C24E'});
	                break;
	            default:
	                this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'});
	                break;
	        }
	    },
	    callback: {
	        onShow : function() {
	            $.noty.themes.defaultTheme.helpers.borderFix.apply(this);
	        },
	        onClose: function() {
	            $.noty.themes.defaultTheme.helpers.borderFix.apply(this);
	        }
	    }
	};

	$.noty.themes.relax = {
	    name    : 'relax',
	    helpers : {},
	    modal   : {
	        css: {
	            position       : 'fixed',
	            width          : '100%',
	            height         : '100%',
	            backgroundColor: '#000',
	            zIndex         : 10000,
	            opacity        : 0.6,
	            display        : 'none',
	            left           : 0,
	            top            : 0
	        }
	    },
	    style   : function() {

	        this.$bar.css({
	            overflow    : 'hidden',
	            margin      : '4px 0',
	            borderRadius: '2px'
	        });

	        this.$message.css({
	            fontSize  : '14px',
	            lineHeight: '16px',
	            textAlign : 'center',
	            padding   : '10px',
	            width     : 'auto',
	            position  : 'relative'
	        });

	        this.$closeButton.css({
	            position  : 'absolute',
	            top       : 4, right: 4,
	            width     : 10, height: 10,
	            background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAxUlEQVR4AR3MPUoDURSA0e++uSkkOxC3IAOWNtaCIDaChfgXBMEZbQRByxCwk+BasgQRZLSYoLgDQbARxry8nyumPcVRKDfd0Aa8AsgDv1zp6pYd5jWOwhvebRTbzNNEw5BSsIpsj/kurQBnmk7sIFcCF5yyZPDRG6trQhujXYosaFoc+2f1MJ89uc76IND6F9BvlXUdpb6xwD2+4q3me3bysiHvtLYrUJto7PD/ve7LNHxSg/woN2kSz4txasBdhyiz3ugPGetTjm3XRokAAAAASUVORK5CYII=)",
	            display   : 'none',
	            cursor    : 'pointer'
	        });

	        this.$buttons.css({
	            padding        : 5,
	            textAlign      : 'right',
	            borderTop      : '1px solid #ccc',
	            backgroundColor: '#fff'
	        });

	        this.$buttons.find('button').css({
	            marginLeft: 5
	        });

	        this.$buttons.find('button:first').css({
	            marginLeft: 0
	        });

	        this.$bar.on({
	            mouseenter: function() {
	                $(this).find('.noty_close').stop().fadeTo('normal', 1);
	            },
	            mouseleave: function() {
	                $(this).find('.noty_close').stop().fadeTo('normal', 0);
	            }
	        });

	        switch(this.options.layout.name) {
	            case 'top':
	                this.$bar.css({
	                    borderBottom: '2px solid #eee',
	                    borderLeft  : '2px solid #eee',
	                    borderRight : '2px solid #eee',
	                    borderTop   : '2px solid #eee',
	                    boxShadow   : "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                break;
	            case 'topCenter':
	            case 'center':
	            case 'bottomCenter':
	            case 'inline':
	                this.$bar.css({
	                    border   : '1px solid #eee',
	                    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                this.$message.css({fontSize: '13px', textAlign: 'center'});
	                break;
	            case 'topLeft':
	            case 'topRight':
	            case 'bottomLeft':
	            case 'bottomRight':
	            case 'centerLeft':
	            case 'centerRight':
	                this.$bar.css({
	                    border   : '1px solid #eee',
	                    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                this.$message.css({fontSize: '13px', textAlign: 'left'});
	                break;
	            case 'bottom':
	                this.$bar.css({
	                    borderTop   : '2px solid #eee',
	                    borderLeft  : '2px solid #eee',
	                    borderRight : '2px solid #eee',
	                    borderBottom: '2px solid #eee',
	                    boxShadow   : "0 -2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                break;
	            default:
	                this.$bar.css({
	                    border   : '2px solid #eee',
	                    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
	                });
	                break;
	        }

	        switch(this.options.type) {
	            case 'alert':
	            case 'notification':
	                this.$bar.css({backgroundColor: '#FFF', borderColor: '#dedede', color: '#444'});
	                break;
	            case 'warning':
	                this.$bar.css({backgroundColor: '#FFEAA8', borderColor: '#FFC237', color: '#826200'});
	                this.$buttons.css({borderTop: '1px solid #FFC237'});
	                break;
	            case 'error':
	                this.$bar.css({backgroundColor: '#FF8181', borderColor: '#e25353', color: '#FFF'});
	                this.$message.css({fontWeight: 'bold'});
	                this.$buttons.css({borderTop: '1px solid darkred'});
	                break;
	            case 'information':
	                this.$bar.css({backgroundColor: '#78C5E7', borderColor: '#3badd6', color: '#FFF'});
	                this.$buttons.css({borderTop: '1px solid #0B90C4'});
	                break;
	            case 'success':
	                this.$bar.css({backgroundColor: '#BCF5BC', borderColor: '#7cdd77', color: 'darkgreen'});
	                this.$buttons.css({borderTop: '1px solid #50C24E'});
	                break;
	            default:
	                this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'});
	                break;
	        }
	    },
	    callback: {
	        onShow : function() {

	        },
	        onClose: function() {

	        }
	    }
	};


	return window.noty;

	});

/***/ },
/* 38 */
/***/ function(module, exports) {

	module.exports = window.jQuery;

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _vue = __webpack_require__(40);

	var _vue2 = _interopRequireDefault(_vue);

	var _log = __webpack_require__(41);

	var _log2 = _interopRequireDefault(_log);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = new _vue2.default({
	  el: '#course',
	  data: {
	    department: Sms.department
	  },
	  computed: {
	    courses: function courses() {
	      return this.department.courses.data.map(function (course) {
	        course.selected = course.pivot.optional === '0';
	        return course;
	      });
	    },
	    selectedCourses: function selectedCourses() {
	      var courses = this.courses;
	      courses = courses.filter(function (course) {
	        return course.selected === true;
	      });

	      return courses;
	    }
	  },
	  created: function created() {},

	  methods: {
	    clickSubmitCourses: function clickSubmitCourses(ev) {
	      ev.preventDefault();
	      (0, _log2.default)(this.courses);
	    }
	  },
	  filters: {
	    log: _log2.default
	  }
	});

/***/ },
/* 40 */
/***/ function(module, exports) {

	module.exports = window.Vue;

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _stringify = __webpack_require__(42);

	var _stringify2 = _interopRequireDefault(_stringify);

	exports.default = function (value) {
	  console.log(JSON.parse((0, _stringify2.default)(value)));
	};

	var _vue = __webpack_require__(40);

	var _vue2 = _interopRequireDefault(_vue);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(43), __esModule: true };

/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	var core  = __webpack_require__(8)
	  , $JSON = core.JSON || (core.JSON = {stringify: JSON.stringify});
	module.exports = function stringify(it){ // eslint-disable-line no-unused-vars
	  return $JSON.stringify.apply($JSON, arguments);
	};

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _vue = __webpack_require__(40);

	var _vue2 = _interopRequireDefault(_vue);

	var _lodash = __webpack_require__(45);

	var _lodash2 = _interopRequireDefault(_lodash);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var courseList = Sms.subjects;

	exports.default = new _vue2.default({
	  el: '#education',
	  data: {
	    fieldsFirst: {
	      course: '',
	      grade: null
	    },
	    fieldsSecond: {
	      course: '',
	      grade: null
	    },
	    grades: ['A1', 'B2', 'B3', 'C4', 'C5', 'C6', 'D7', 'E8', 'F9'],
	    selectedCoursesFirst: [],
	    selectedCoursesSecond: [],
	    show: false
	  },

	  computed: {
	    availableCoursesFirst: function availableCoursesFirst() {
	      var selectedIds = _lodash2.default.map(this.selectedCoursesFirst, 'id');
	      return _lodash2.default.clone(courseList).filter(function (course) {
	        return selectedIds.indexOf(course.id) === -1;
	      });
	    },
	    availableCoursesSecond: function availableCoursesSecond() {
	      var selectedIds = _lodash2.default.map(this.selectedCoursesSecond, 'id');
	      return _lodash2.default.clone(courseList).filter(function (course) {
	        return selectedIds.indexOf(course.id) === -1;
	      });
	    }
	  },

	  methods: {
	    clickRemoveCourseFirst: function clickRemoveCourseFirst(id) {
	      var idx = _lodash2.default.findIndex(this.selectedCoursesFirst, { id: Number(id) });

	      this.selectedCoursesFirst.splice(idx, 1);
	    },
	    clickRemoveCourseSecond: function clickRemoveCourseSecond(id) {
	      var idx = _lodash2.default.findIndex(this.selectedCoursesSecond, { id: Number(id) });

	      this.selectedCoursesSecond.splice(idx, 1);
	    },
	    clickAddCourseFirst: function clickAddCourseFirst() {
	      if (this.fieldsFirst.course === '') {
	        return void 0;
	      }

	      var course = _lodash2.default.find(courseList, { id: Number(this.fieldsFirst.course) });

	      this.selectedCoursesFirst.push({
	        id: course.id,
	        name: course.name,
	        grade: this.fieldsFirst.grade
	      });

	      this.fieldsFirst.course = '';
	      this.fieldsFirst.grade = null;
	    },
	    clickAddCourseSecond: function clickAddCourseSecond() {
	      if (this.fieldsSecond.course === '') {
	        return void 0;
	      }

	      var course = _lodash2.default.find(courseList, { id: Number(this.fieldsSecond.course) });

	      this.selectedCoursesSecond.push({
	        id: course.id,
	        name: course.name,
	        grade: this.fieldsSecond.grade
	      });

	      this.fieldsSecond.course = '';
	      this.fieldsSecond.grade = null;
	    },
	    addSitting: function addSitting() {
	      return this.show = true;
	    },
	    removeSitting: function removeSitting() {
	      return this.show = false;
	    }
	  }
	});

/***/ },
/* 45 */
/***/ function(module, exports) {

	module.exports = window._;

/***/ }
/******/ ]);