'use strict'

const gulp = require('gulp')
const gulpConcat = require('gulp-concat')
const gulpSass = require('gulp-sass')
const gulpCleanCss = require('gulp-clean-css')
const webpack = require('webpack-stream')
const vinylNamed = require('vinyl-named')
const cfg = require('./gulpfile.config')

function vendor() {
  let promises = []

  promises.push(new Promise(function (resolve, reject) {
    gulp.src(cfg.vendor.scripts)
    //.pipe($.plumber(cfg.plumber))
      .pipe(gulpConcat('vendor.js'))
      //.pipe($.if(cfg.opts.production, $.uglify(cfg.uglify)))
      .pipe(gulp.dest('public/assets/js'))
      .on('error', reject)
      .on('end', resolve)
  }))

  promises.push(new Promise(function (resolve, reject) {
    gulp.src(cfg.vendor.styles)
    //.pipe($.plumber(cfg.plumber))
      .pipe(gulpConcat('vendor.css'))
      //.pipe($.if(cfg.opts.production, $.cleanCss()))
      //.pipe($.postcss(cfg.postcss.processors))
      .pipe(gulp.dest('public/assets/css'))
      .on('error', reject)
      .on('end', resolve)
  }))

  return Promise.all(promises)
}

function styles() {
  return gulp.src('resources/assets/scss/app.scss')
    .pipe(gulpSass())
    .pipe(gulpConcat('app.css'))
    .pipe(gulpCleanCss())
    .pipe(gulp.dest('public/assets/css'))
}

function scripts() {
  return gulp.src('resources/assets/js/app.js')
    .pipe(vinylNamed())
    .pipe(webpack(cfg.webpack()))
    .pipe(gulp.dest('public/assets/js'))
}

gulp.task('styles', styles)
gulp.task('scripts', scripts)
gulp.task('vendor', vendor)
gulp.task('build', gulp.parallel('vendor', 'scripts', 'styles'))

gulp.task('watch', gulp.series('build', function watcher() {
  gulp.watch('resources/assets/scss/**/*', {}, styles)
  gulp.watch('resources/assets/js/**/*', {}, scripts)
}))
