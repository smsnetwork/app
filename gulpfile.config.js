'use strict'

const path = require('path')
const webpack = require('webpack')

exports.production = false

exports.webpack = function () {
  return {
    output: {
      filename: '[name].js'
    },
    module: {
      loaders: [{
        test: /\.js$/,
        include: [path.join(__dirname, 'resources/assets/js')],
        loader: 'babel',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }]
    },
    get plugins() {
      let plugins = []

      if (exports.production) {
        plugins.push(new webpack.DefinePlugin({
          'process.env.NODE_ENV': '"production"'
        }))

        plugins.push(new webpack.optimize.UglifyJsPlugin({
          compress: {
            warnings: false
          }
        }))
      }

      return plugins
    },
    externals: {
      'jquery': 'window.jQuery',
      'vue': 'window.Vue',
      'lodash': 'window._',
      //'moment': 'window.moment',
      //'noty': 'window.noty'
    },
    resolve: {
      alias: {},
      extensions: ['', '.js', '.vue', '.json', '.css'],
      fallback: [path.join(__dirname, 'node_modules')]
    }
  }
}

exports.vendor = {
  styles: [
    'resources/assets/libs/bootstrap/dist/css/bootstrap.css',
    'resources/assets/libs/bootstrap/dist/css/bootstrap-datepicker.css',
    'resources/assets/libs/font-awesome/css/font-awesome.css',
    'resources/assets/libs/animate/css/animate.css',
  ],
  scripts: [
    'resources/assets/libs/jquery/jquery.js',
    'resources/assets/libs/tether/dist/js/tether.js',
    'resources/assets/libs/bootstrap/dist/js/bootstrap.js',
    'resources/assets/libs/theme/scripts.js',
    'resources/assets/libs/theme/nanoscroller.js',
    'resources/assets/libs/theme/metismenu.js',
    'resources/assets/libs/vue/vue.js',
    'resources/assets/libs/lodash/lodash.js',
    'resources/assets/libs/bootstrap/dist/js/bootstrap-datepicker.js',
    'resources/assets/libs/noty/packaged/jquery.noty.packaged.js',
  ],
  fonts: [
    'resources/assets/libs/font-awesome/fonts/**',
    'resources/assets/libs/bootstrap/dist/fonts/**'
  ],
  images: [
    //'resources/assets/libs/datatables/img/**/*'
  ]
}
